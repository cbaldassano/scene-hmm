function RecallSegments()

load('sherlock_pcc_data_and_timestamps','ev');
nSubj = 17;
movie_TRs = cell(nSubj,1);
recall_TRs = cell(nSubj,1);
for s = 1:17
    recallannot = ev.(['s' num2str(s)]).events.freerecall;
    movieannot = [ev.(['s' num2str(s)]).events.SL1;945+ev.(['s' num2str(s)]).events.SL2];
    scenenum = ev.(['s' num2str(s)]).events.freerecallnum;
    
    mf = matfile(fullfile('Janice_PCC',['s' num2str(s)]));
    recall_length = size(mf.recall,2);
    movie_TRs{s} = zeros(1976,1);
    recall_TRs{s} = zeros(recall_length,1);
    for i = 1:length(scenenum)
        movie_TRs{s}(movieannot(i,1):movieannot(i,2)) = scenenum(i);
        recall_TRs{s}(recallannot(i,1):recallannot(i,2)) = scenenum(i);
    end
end

save(fullfile('Janice_PCC','recall_segments'),'movie_TRs','recall_TRs');
end