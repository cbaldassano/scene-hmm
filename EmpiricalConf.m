function bounds = EmpiricalConf(x, interval)
if (nargin < 2)
    interval = [0.025 0.975];
end
sort_x = sort(x,2,'ascend');
bounds = sort_x(:,round(interval*(length(x)-1))+1);
end