#!/usr/bin/env bash

#SBATCH -t 60
#SBATCH --mem 1000
#SBATCH -p all
#SBATCH -c 2

module load matlab/R2015a
matlab -nosplash -nojvm -nodisplay -nodesktop -r "try; LOO_acrossModality_copyBound('$loo','$nScenes'); catch me; fprintf('%s / %s\n',me.identifier,me.message); end; exit"
