function segcorr = PairwiseSegmentCorr(boundaries1, boundaries2, maxT)
boundaries1 = [1;boundaries1(:);maxT];
boundaries2 = [1;boundaries2(:);maxT];

mat1 = zeros(maxT,maxT);
mat2 = zeros(maxT,maxT);

for i = 1:(length(boundaries1)-1)
    mat1(boundaries1(i):boundaries1(i+1),boundaries1(i):boundaries1(i+1)) = 1;
end
for i = 1:(length(boundaries2)-1)
    mat2(boundaries2(i):boundaries2(i+1),boundaries2(i):boundaries2(i+1)) = 1;
end

segcorr = corr2(mat1,mat2);
end