function zvec = NullZscore(true_vec, null_vecs)
% True vec is Nx1, null_vecs is NxP
% N=scenes, P=permutations

zvec = zeros(length(true_vec),1);
for i = 1:length(zvec)
    nullmean = mean(null_vecs(i,:));
    nullstd = std(null_vecs(i,:));
    zvec(i) = (true_vec(i)-nullmean)/nullstd;
end

end