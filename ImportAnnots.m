function [diffs, diff_titles, scene_characters] = ImportAnnots()
[xls_num, xls_text] = xlsread('Janice\Sherlock_Segments_master 032716.xlsx');
xls_num = [zeros(1,20);xls_num];
diff_titles = {'Scenes_RA','Scenes_Janice','IndoorOutdoor', 'Location', 'Music', 'Temporal'};
diffs = zeros(length(diff_titles),1976);

xls_text = cellfun(@strtrim, xls_text, 'UniformOutput', false);

scene_characters = {{}};

for line = 2:998
    % Scenes_RA
    if(isempty(xls_text{line-1,7}) && ~isempty(xls_text{line,7}))
        diffs(1,xls_num(line,5)) = 1;
        scene_characters{end+1} = {};
    end
    
    scene_characters{end} = union(scene_characters{end}, cellfun(@strtrim, strsplit(xls_text{line,11},','), 'UniformOutput', false));
    scene_characters{end} = setdiff(scene_characters{end},{'Nobody'});
    scene_characters{end} = scene_characters{end}(:);
    
    % Scenes_Janice
    if(isempty(xls_text{line-1,8}) && ~isempty(xls_text{line,8}))
        diffs(2,xls_num(line,5)) = 1;
    end
    
     % IndoorOutdoor
    if(line>2 && ~strcmp(xls_text{line-1,9},xls_text{line,9}))
        diffs(3,xls_num(line,5)) = 1;
    end
    
    % Location
    if(line>2 && ~strcmp(xls_text{line-1,14},xls_text{line,14}))
        diffs(4,xls_num(line,5)) = 1;
    end
    
    % Music
    if(line>2 && ~strcmp(xls_text{line-1,17},xls_text{line,17}))
        diffs(5,xls_num(line,5)) = 1;
    end
    
    % Temporal
    if(isempty(xls_text{line-1,18}) && ~isempty(xls_text{line,18}))
        diffs(6,xls_num(line,5)) = 1;
    end
end
end