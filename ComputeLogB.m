function logB = ComputeLogB(target_data, mean_patterns, iteration_var)
    nDim = size(mean_patterns,1);
    K = size(mean_patterns,2);
    
    target_data = zscore(target_data);
    mean_patterns = zscore(mean_patterns);
    
    logB = zeros(size(target_data,2), K);
    
    if length(iteration_var) == 1
        iteration_var = iteration_var*ones(K,1);
    end

    for k = 1:K
        % Assumes isotropic variance for all features
        logB(:,k) = -0.5 * nDim*log(2*pi*iteration_var(k))-...
            0.5*sum(bsxfun(@minus,target_data',mean_patterns(:,k)').^2,2)/iteration_var(k);
    end
    
    % Normalize dimension (equiv to scaling by 1/sqrt(nDim))
    logB = logB/nDim;
end