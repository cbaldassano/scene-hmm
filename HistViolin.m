function maxp = HistViolin(x, Y, edges, col, maxp)
% A histogram of each column of Y
if (nargin < 4)
    col = [0.6 0.6 0.6];
end

if (nargin < 5)
    maxp = -Inf;
    for i = 1:size(Y,2)
        p = histcounts(Y(:,i),edges,'Normalization','pdf');
        if (max(p) > maxp)
            maxp = max(p);
        end
    end
end

hold on;
for i = 1:size(Y,2)
    p = histcounts(Y(:,i),edges,'Normalization','pdf');
    p = p/maxp*0.9*(x(2)-x(1));
    xvec = x(i);
    yvec = edges(1);
    for j = 1:length(p)
        xvec = [xvec p(j)+x(i) p(j)+x(i)];
        yvec = [yvec edges(j) edges(j+1)];
    end
    xvec = [xvec x(i) x(i)];
    yvec = [yvec edges(end) edges(1)];
    
    fill(xvec,yvec,col,'EdgeColor','none');
end
hold off;

end