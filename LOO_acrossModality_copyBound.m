function LOO_acrossModality_copyBound(loo, nScenes)

nScram = 100;
dirname = 'TO_LOO_acrossModality_copyBound';
loo = str2double(loo);
if (strcmp('22Annot',nScenes))
    nScenes = 22;
    use_human_annot = true;
else
    nScenes = str2double(nScenes);
    use_human_annot = false;
end

tic;
experiment = 'Asieh'; %'Janice';%
datasets = {'SherlockAudio1' 'SherlockAudio2'};%{'Recall'};%
rois = {'A1' 'PMC'};%{'PCC'};%
test_LL = zeros(nScram+1,length(datasets),length(rois));
meancorr = zeros(nScram+1,length(datasets),length(rois));
for dataset_ind = 1:length(datasets)
    for roi_ind = 1:length(rois)
        loaded = load(fullfile(experiment,'SherlockMovie',[rois{roi_ind} '.mat'])); %Movie
        movie = loaded.(rois{roi_ind});
        clear loaded;

        loaded = load(fullfile(experiment,datasets{dataset_ind},[rois{roi_ind} '.mat']));
        audio = loaded.(rois{roi_ind});
        TA = size(audio,2);
        clear loaded;

        train_subj = setdiff(1:size(movie,3),loo);
        group_train = mean(movie(:,:,train_subj),3);
        

        if (use_human_annot)
            loaded = load(fullfile(experiment,'SherlockMovie','segment'));
            movie_seg = loaded.Sherlock_movie_segment;
            movie_gamma = LabelToGamma(movie_seg);
        else
            % Learn (soft) segmentation on all but one subject
            [~, ~, ~, loggamma] = HMM_BW('trainingData',group_train,'nScenes',nScenes);
            movie_gamma = exp(loggamma);
        end
        
        % First is real, rest are scrambles
        gammas = zeros(TA,nScenes,nScram+1);
        
        sceneV = SceneVariance(movie(:,:,train_subj),movie_gamma);
        [~, ~, ~, loggamma_audio] = ...
            HMM_BW('trainingData',group_train,'trainingSegments',movie_gamma,'sceneVariance',sceneV, 'testingData',mean(audio(:,:,train_subj),3));
        gammas(:,:,1) = exp(loggamma_audio);

        % Generate random segmentations with same scene length distribution
        [~,maxscene] = max(gammas(:,:,1),[],2);
        t = tabulate(maxscene);
        sceneSizes = t(:,2);
        rng(1);
        for i = 1:nScram
            borders = [0;cumsum(sceneSizes(randperm(nScenes)))];
            for k = 1:nScenes
                gammas((borders(k)+1):borders(k+1),k,i+1) = 1;
            end
        end

        for i = 1:(nScram+1)
            test_LL(i,dataset_ind,roi_ind) = ...
                HMM_BW('trainingData',group_train,'trainingSegments',movie_gamma,'sceneVariance',sceneV, ...
                            'testingData',audio(:,:,loo),'testingSegments',gammas(:,:,i));
                        
            % Calculate scene correlations with group
            meanPatterns_group =  group_train*bsxfun(@times,movie_gamma,1./sum(movie_gamma,1));
            meanPatterns_loo =  audio(:,:,loo)*bsxfun(@times,gammas(:,:,i),1./sum(gammas(:,:,i),1));
            meancorr(i,dataset_ind,roi_ind) = mean(corr_columns(meanPatterns_group,meanPatterns_loo),'omitnan');
        end

        disp([datasets{dataset_ind} ' ' rois{roi_ind} ' ' num2str(test_LL(1,dataset_ind,roi_ind)) ' ' num2str(meancorr(1,dataset_ind,roi_ind)) ' ' num2str(mean(test_LL(2:end,dataset_ind,roi_ind))) ' ' num2str(mean(meancorr(2:end,dataset_ind,roi_ind)))]);
        toc;
        if ~exist(dirname,'dir')
            mkdir(dirname);
        end
        if (use_human_annot)
            filename = [num2str(loo) '_' num2str(nScenes) 'Annot.mat'];
        else
            filename = [num2str(loo) '_' num2str(nScenes) '.mat'];
        end
        save([dirname '/' filename],'test_LL','meancorr');
    end
end

end