function PlotLOO()

% %% Within modality
% nScenes = [2:10:52 62:20:282];
% meancorr = zeros(length(nScenes),17,101,3,2);
% test_LL = zeros(length(nScenes),17,101,3,2);
% for s = 1:length(nScenes)
%     for l = 1:17
%         loaded = load(fullfile('TO_LOO_withinModality',[num2str(l) '_' num2str(nScenes(s))]));
%         meancorr(s,l,:,:,:) = loaded.meancorr;
%         test_LL(s,l,:,:,:) = loaded.test_LL;
%     end
% end
% meancorr = squeeze(mean(meancorr,2));
% test_LL = squeeze(mean(test_LL,2));
% 
% f1 = figure;
% %f2 = figure;
% data = {'Movie' 'Naive listeners' 'Memory listeners'};
% ROI = {'A1' 'PMC'};
% for i = 1:2
%     t = ceil(i/2);
%     r = mod(i-1,2)+1;
%     
%     figure(f1);
%     subplot(2,2,i);
%     plot(nScenes, test_LL(:,1,t,r)-mean(test_LL(:,2:end,t,r),2));%, nScenes, nullconf(meancorr(:,2:end,t,r),0.01)-mean(meancorr(:,2:end,t,r),2));
%     xlabel('Scenes');
%     title([data{t} ', ' ROI{r} ' log-likelihood minus null']);
%     ylim([0 max(test_LL(:,1,t,r)-mean(test_LL(:,2:end,t,r),2))]);
%     subplot(2,2,i+2);
%     plot(nScenes, meancorr(:,1,t,r)-mean(meancorr(:,2:end,t,r),2));%, nScenes, nullconf(test_LL(:,2:end,t,r),0.01)-mean(test_LL(:,2:end,t,r),2));
%     xlabel('Scenes');
%     title([data{t} ', ' ROI{r} ' correlation minus null']);
%     ylim([0 max(meancorr(:,1,t,r)-mean(meancorr(:,2:end,t,r),2))]);
%     
%     
% %     figure(f1);
% %     subplot(3,2,i);
% %     shadedErrorBar(nScenes, mean(meancorr(:,2:end,t,r),2), [(nullconf(meancorr(:,2:end,t,r),0.01)-mean(meancorr(:,2:end,t,r),2))';mean(meancorr(:,2:end,t,r),2)']);
% %     hold on;
% %     plot(nScenes, meancorr(:,1,t,r));
% %     xlabel('Scenes');
% %     title([data{t} ', ' ROI{r} ' correlation']);
% %     
% % %     figure(f2);
% % %     subplot(3,2,i);
% % %     plot(nScenes, meancorr(:,1,t,r)./mean(meancorr(:,2:end,t,r),2));
% % %     xlabel('Scenes');
% % %     title([data{t} ', ' ROI{r} ' correlation increase']);
% % %     
% %     figure(f2);
% %     subplot(3,2,i);
% %     plot(nScenes, test_LL(:,1,t,r)-mean(test_LL(:,2:end,t,r),2), nScenes, nullconf(test_LL(:,2:end,t,r))-mean(test_LL(:,2:end,t,r),2));
% %     legend('Log-likelihood vs null','p=0.01 null');
% %     xlabel('Scenes');
% %     title([data{t} ', ' ROI{r}]);
% end
% 
%% Across modality
nScenes = [2:10:52 62:20:142];
meancorr = zeros(length(nScenes)+1,17,101,2,2);
test_LL = zeros(length(nScenes)+1,17,101,2,2);
for s = 1:(length(nScenes)+1)
    for l = 1:17
        if (s == length(nScenes)+1)
            loaded = load(fullfile('TO_LOO_acrossModality',[num2str(l) '_22Annot']));
        else
            loaded = load(fullfile('TO_LOO_acrossModality',[num2str(l) '_' num2str(nScenes(s))]));
        end
        meancorr(s,l,:,:,:) = loaded.meancorr;
        test_LL(s,l,:,:,:) = loaded.test_LL;
    end
end
meancorr = squeeze(mean(meancorr,2));
test_LL = squeeze(mean(test_LL,2));

f1 = figure;
%f2 = figure;
target = {'Naive listeners' 'Memory listeners'};
ROI = {'A1' 'PMC'};
for i = 3:4
    t = ceil(i/2);
    r = mod(i-1,2)+1;
    
    figure(f1);
    subplot(2,2,i-2);
    plot(nScenes, test_LL(1:(end-1),1,t,r)-mean(test_LL(1:(end-1),2:end,t,r),2), nScenes, nullconf(test_LL(1:(end-1),2:end,t,r),0.01)-mean(test_LL(1:(end-1),2:end,t,r),2));
    xlabel('Scenes');
    title([target{t} ', ' ROI{r} ' log-likelihood minus null']);
    curry = get(gca,'Ylim');
    ylim([0 curry(2)]);
%     hold on;
%     scatter(22,test_LL(end,1,t,r)-mean(test_LL(end,2:end,t,r)),10,[0 114 189]/255,'filled');
    
    subplot(2,2,i);
    plot(nScenes, meancorr(1:(end-1),1,t,r)-mean(meancorr(1:(end-1),2:end,t,r),2), nScenes, nullconf(meancorr(1:(end-1),2:end,t,r),0.01)-mean(meancorr(1:(end-1),2:end,t,r),2));
    xlabel('Scenes');
    title([target{t} ', ' ROI{r} ' correlation minus null']);
    curry = get(gca,'Ylim');
    ylim([0 curry(2)]);
%     hold on;
%     scatter(22,meancorr(end,1,t,r)-mean(meancorr(end,2:end,t,r)),10,[0 114 189]/255,'filled');
    
%     figure(f1);
%     subplot(2,2,i);
%     shadedErrorBar(nScenes, mean(meancorr(1:(end-1),2:end,t,r),2), [(nullconf(meancorr(1:(end-1),2:end,t,r),0.01)-mean(meancorr(1:(end-1),2:end,t,r),2))';mean(meancorr(1:(end-1),2:end,t,r),2)']);
%     hold on;
%     plot(nScenes, meancorr(1:(end-1),1,t,r));
%     scatter(22,meancorr(end,1,t,r),10,[237 177 32]/255,'filled');
%     scatter(22,mean(meancorr(end,2:end,t,r)),10,'k','filled');
%     legend('Pattern correlation','p=0.01 null');
%     xlabel('Scenes');
%     title(['Movie -> ' target{t} ', ' ROI{r} ' correlation']);
    
%     figure(f2);
%     subplot(2,2,i);
%     plot(nScenes, meancorr(:,1,t,r)./mean(meancorr(:,2:end,t,r),2));
%     xlabel('Scenes');
%     title(['Movie -> ' target{t} ', ' ROI{r} ' correlation increase']);
%     figure(f2);
%     subplot(2,2,i);
%     plot(nScenes, test_LL(:,1,t,r)-mean(test_LL(:,2:end,t,r),2), nScenes, nullconf(test_LL(:,2:end,t,r))-mean(test_LL(:,2:end,t,r),2));
%     legend('Log-likelihood vs null','p=0.01 null');
%     xlabel('Scenes');
%     title(['Movie -> ' target{t} ', ' ROI{r}]);
end

figure;
plot(nScenes(1:8), NullZscore(test_LL(1:8,1,1,1),test_LL(1:8,2:end,1,1)), ...
     nScenes(1:8), NullZscore(test_LL(1:8,1,1,2),test_LL(1:8,2:end,1,2)));
line([0 100],2.495*[1 1]);
xlabel('Number of scenes');
ylabel('Likelihood vs. null (Z)');
box off
legend('Auditory','Posterior Medial');
xlim([2 82]);

% %% SimulTrain
% nScenes = [2:10:42 62 82];
% nPerm = 100;
% meancorr = zeros(length(nScenes),17,nPerm+1,2,2,2);
% test_LL = zeros(length(nScenes),17,nPerm+1,2,2,2);
% for s = 1:length(nScenes)
%     for l = 1:17
%         loaded = load(fullfile('TO_LOO_simulTrain',[num2str(l) '_' num2str(nScenes(s))]));
%         meancorr(s,l,:,:,:,:) = loaded.meancorr;
%         test_LL(s,l,:,:,:,:) = loaded.test_LL;
%     end
% end
% meancorr = squeeze(mean(meancorr,2));
% test_LL = squeeze(mean(test_LL,2));
% 
% %f1 = figure;
% f2 = figure;
% target = {'Naive listeners' 'Memory listeners'};
% ROI = {'A1' 'PMC'};
% for i = 3:4
%     t = ceil(i/2);
%     r = mod(i-1,2)+1;
%     
% %     figure(f1);
% %     subplot(2,2,i);
% %     plot(nScenes, meancorr(:,1,t,r,1), nScenes, nullconf(meancorr(:,2:end,t,r,1)),nScenes, meancorr(:,1,t,r,2), nScenes, nullconf(meancorr(:,2:end,t,r,2)));
% %     legend('Pattern correlation (movie)','p=0.01 null (movie)','Pattern correlation (audio)','p=0.01 null (audio)');
% %     xlabel('Scenes');
% %     title(['Movie and ' target{t} ', ' ROI{r}]);
%     
%     figure(f2);
%     subplot(1,2,i-2);
%     plot(nScenes, mean(test_LL(:,1,t,r,:),5)-mean(mean(test_LL(:,2:end,t,r,:),5),2), nScenes, nullconf(mean(test_LL(:,2:end,t,r,:),5))-mean(mean(test_LL(:,2:end,t,r,:),5),2));
%     legend('Log-likelihood vs null','p=0.01 null');
%     xlabel('Scenes');
%     title(['Movie and ' target{t} ', ' ROI{r}]);
% end

%% Across Modality - copy bound
nScenes = 32;%2:10:52;
nPerm = 100;
meancorr = zeros(length(nScenes),17,nPerm+1,2,2);
test_LL = zeros(length(nScenes),17,nPerm+1,2,2);
for s = 1:length(nScenes)
    for l = 1:17
        loaded = load(fullfile('TO_LOO_acrossModality_copyBound',[num2str(l) '_' num2str(nScenes(s))]));
        meancorr(s,l,:,:,:,:) = loaded.meancorr;
        test_LL(s,l,:,:,:,:) = loaded.test_LL;
    end
end
meancorr = squeeze(mean(meancorr,2));
test_LL = squeeze(mean(test_LL,2));

f1 = figure;
target = {'Naive listeners' 'Memory listeners'};
ROI = {'A1' 'PMC'};
for i = 3:4
    t = ceil(i/2);
    r = mod(i-1,2)+1;
    
    figure(f1);
    subplot(2,2,i-2);
    plot(nScenes, test_LL(:,1,t,r)-mean(test_LL(:,2:end,t,r),2), nScenes, nullconf(test_LL(:,2:end,t,r),0.01)-mean(test_LL(:,2:end,t,r),2));
    xlabel('Scenes');
    title([target{t} ', ' ROI{r} ' log-likelihood minus null']);
    curry = get(gca,'Ylim');
    ylim([0 curry(2)]);
    
    subplot(2,2,i);
    plot(nScenes, meancorr(:,1,t,r)-mean(meancorr(:,2:end,t,r),2), nScenes, nullconf(meancorr(:,2:end,t,r),0.01)-mean(meancorr(:,2:end,t,r),2));
    xlabel('Scenes');
    title([target{t} ', ' ROI{r} ' correlation minus null']);
    curry = get(gca,'Ylim');
    ylim([0 curry(2)]);
end

end

function t = nullconf(x,p,tail)
if (nargin < 2)
    p = 0.01;
end
if (nargin < 3)
    tail = 'right';
end
sorted = sort(x,2,'ascend');
if (strcmp(tail,'right'))
    t = sorted(:,round(size(sorted,2)*(1-p)));
elseif (strcmp(tail,'left'))
    t = sorted(:,round(size(sorted,2)*p));
end
end