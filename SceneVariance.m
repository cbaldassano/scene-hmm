function V = SceneVariance(data, gamma)
% data is VxTxS
% gamma is TxK
% Calculates variance (per dim) for each scene, pooled across time and subjects

meanpat = mean(data,3)*bsxfun(@times,gamma,1./sum(gamma,1));
meanpat = zscore(meanpat);
data = zscore(data,[],1);
V = zeros(size(gamma,2),1);
for k = 1:size(gamma,2)
    nonzero_ind = gamma(:,k)>0.01;
    scenegamma = repmat(gamma(nonzero_ind,k),1,size(data,3));
    sumsqs = sum(sum(scenegamma.*shiftdim(sum(bsxfun(@minus,data(:,nonzero_ind,:),meanpat(:,k)).^2,1),1)));
    V(k) = sumsqs*sum(scenegamma(:))/(sum(scenegamma(:))^2-sum(scenegamma(:).^2)); % Unbiased variance normalization
end
V = V/size(data,1); % Normalize by dimension (to match ComputeLogB)
end