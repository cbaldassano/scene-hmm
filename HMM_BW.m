function [start_ll, end_ll, end_scale, end_loggamma, end_viterbi]  = HMM_BW(varargin)
%HMM_BW Fits scene HMM using Baum-Welch or transfers scenes to testing data
%
% Arguments specified as property-value pairs:
% trainingData (required): data for learning scene patterns
%    Voxels by Timepoints matrix, or cell array of multiple such matrices
%
% nScenes (required unless using trainingSegments): number of scenes to
%    find in training data
%
% trainingSegments: initial/learned segmentation of trainingData timepoints
%    Timepoints by scenes (probabilities), or  Timepoints by 1 (labels)
%    Should be a cell array if trainingData is a cell array
%
% testingData: data in which to find scenes after learning scene patterns
%    in trainingData.
%    Voxels by Timepoints matrix, or cell array of multiple such matrices
%
% transType: structure of scene transition matrix
%    'Ordered' for forward-only (default), or 'Full' for all transitions
%
% sceneVariance (required if not performing BW): variance of scene patterns
%    scalar value, or vector of scene variances, or function of number of steps for BW
%    Default is 4*0.98^(step-1)
%
% doubleZscore: whether to perform a double z-scoring (in time then in
%    space), applied before PCA
%    Default is false
%
% PCAdims: dimensionality to reduce timecourses to
%    Default is -1 (disabled)
%
% lockTrainingSegment: only fit variance during BW (not training segment)
%    Default is false
%
% testingSegments: don't run forward-backward on the testing data, just
%    apply this segmentation and measure fit.
%    Default is false
%
% copySegments: don't run forward-backward on testing data, just copy over
%    the training segments and measure fit. Only possible if training and
%    testing data have the same number of timepoints. Equivalent to setting
%    testingSegments=trainingSegments
%    Default is false
%
% scrambleCorrespond: when given two training sets, randomize the
%    correspondence between their scenes
%    Default is false


%% Input validation and standardization
inpParse = inputParser;
inpParse.FunctionName = 'HMM_BW';
inpParse.addParamValue('trainingData',[]);
inpParse.addParamValue('nScenes',[]);
inpParse.addParamValue('trainingSegments',[]);
inpParse.addParamValue('testingData',[]);
inpParse.addParamValue('transType','Ordered');
inpParse.addParamValue('sceneVariance',@(step) 4*0.98^(step-1));
inpParse.addParamValue('doubleZscore',false);
inpParse.addParamValue('PCAdims',-1);
inpParse.addParamValue('lockTrainingSegment',false);
inpParse.addParamValue('testingSegments',[]);
inpParse.addParamValue('copySegments',false);
inpParse.addParamValue('scrambleCorrespond',false);

inpParse.parse(varargin{:});
trainingData = inpParse.Results.trainingData;
nScenes = inpParse.Results.nScenes;
trainingSegments = inpParse.Results.trainingSegments;
testingData = inpParse.Results.testingData;
transType = inpParse.Results.transType;
sceneVariance = inpParse.Results.sceneVariance;
doubleZscore = inpParse.Results.doubleZscore;
PCAdims = inpParse.Results.PCAdims;
lockTrainingSegment = inpParse.Results.lockTrainingSegment;
testingSegments = inpParse.Results.testingSegments;
copySegments = inpParse.Results.copySegments;
scrambleCorrespond = inpParse.Results.scrambleCorrespond;
isUnset = @(y) any(cellfun(@(x) strcmp(x,y),inpParse.UsingDefaults));

if (~isUnset('testingData') && isUnset('sceneVariance'))
    error('If not performing BW, must specify scene variance');
end

if ~isa(sceneVariance, 'function_handle')
    sceneVariance = @(step) sceneVariance;
end

if ~iscell(trainingData)
    trainingData = {trainingData};
end
if ~iscell(trainingSegments)
    trainingSegments = {trainingSegments};
end
nTrainSets = length(trainingData);

if (isUnset('nScenes'))
    if (isUnset('trainingSegments'))
        error('Must specify nScenes or trainingSegments');
    end
    
    assert(length(trainingSegments) == nTrainSets);
    for i = 1:nTrainSets
        if (size(trainingSegments{i},2)==1)
            % Convert to "gamma" with binary entries
            trainingSegments{i} = LabelToGamma(trainingSegments{i});
        end
    end
else
    if (~isUnset('trainingSegments'))
        error('Cannot specify both nScenes and trainingSegments');
    end
    
    if (~isUnset('testingData'))
        error('Need trainingSegments if not performing BW');
    end
    
    trainingSegments = cell(nTrainSets,1);
    for i = 1:nTrainSets
        trainingSegments{i} = ones(size(trainingData{i},2),nScenes);
    end
end

if (doubleZscore)
    for i = 1:nTrainSets
        trainingData{i} = zscore(trainingData{i},[],2);
        trainingData{i} = zscore(trainingData{i});
    end
end

if (PCAdims > 0)
    training_TRs = [0 cumsum(cellfun(@(x) size(x,2), trainingData))];
    warning('off','stats:pca:ColRankDefX');
    [pca_trans,all_training_pca,~,~,~,pca_mu] = pca(cat(2,trainingData{:})','NumComponents', PCAdims);
    warning('on','stats:pca:ColRankDefX');
    for i = 1:nTrainSets
        trainingData{i} = all_training_pca((training_TRs(i)+1):training_TRs(i+1),:)';
    end
end
nDim = size(trainingData{1},1);

if (isUnset('testingData'))
    maxsteps = 500;
    targetData = trainingData;
else
    maxsteps = 1;

    if ~iscell(testingData)
        testingData = {testingData};
    end
    nTestSets = length(testingData);
    
    if (doubleZscore)
        for i = 1:nTestSets
            testingData{i} = zscore(testingData{i},[],2);
            testingData{i} = zscore(testingData{i});
        end
    end

    if (PCAdims > 0)
        for i = 1:nTestSets
            testingData{i} = (bsxfun(@minus,testingData{i}',pca_mu)*pca_trans)';
        end
    end
    targetData = testingData;
end

if (copySegments)
    if (length(trainingData) ~= length(testingData))
        error('Can only copy segments if training/testing have same number of datasets');
    end
    for i = 1:nTrainSets
        if any(size(trainingData{i}) ~= size(testingData{i}))
            error('Can only copy segments if training/testing have same dimensions');
        end
    end
end

if (scrambleCorrespond)
    if (length(trainingData) < 2)
        error('Can only scramble correspondences between multiple training datasets');
    end
    if (isUnset('nScenes'))
        error('Can only scramble correspondences when using nScenes argument');
    end
    scram = zeros(length(trainingData),nScenes);
    for i = 1:length(trainingData)
        scram(i,:) = randperm(nScenes);
    end
end

if (~isempty(testingSegments))
    if ~iscell(testingSegments)
        testingSegments = {testingSegments};
    end
    
    if (length(testingSegments) ~= length(testingData))
        error('Number of testing segments must equal number of test datasets');
    end
    for i = 1:nTestSets
        if (size(testingData{i},2) ~= size(testingSegments{i},1))
            error('Testing segments must have same dimensions as testing data');
        end
    end
end

%% Setting up transition matrix
K = size(trainingSegments{1},2);
if strcmp(transType,'Ordered')
    % Stay or advance, with final sink state
    Pi = [1 zeros(1,K-1)];
    P = [0.5*diag(ones(K,1)) + 0.5*diag(ones(K-1,1),1) [zeros(K-1,1);0.5]];
    EndPi = [zeros(1,K-1) 1];
elseif strcmp(transType,'Full')
    % All moves allowed
    stayprob = transType;
    offprob = (1-stayprob)/(K-1);
    P = diag((stayprob-offprob)*(ones(K,1))) + offprob*ones(K);
    Pi = 1/K*ones(1,K);
    EndPi = 1/K*ones(1,K);
else
    error('Unknown transition type');
end

%% Main fitting loop
stepnum = 1;
ll_hist = zeros(0,length(targetData));
end_ll = -Inf*ones(length(targetData),1);

while (stepnum <= maxsteps)
    iterationVar = sceneVariance(stepnum);

    % Compute mean patterns based on current segmentation
    meanPatterns = zeros(nTrainSets,nDim,K);
    for i = 1:nTrainSets
        meanPatterns(i,:,:) = trainingData{i}*bsxfun(@times,trainingSegments{i},1./sum(trainingSegments{i},1));
    end
    meanPatterns = squeeze(mean(meanPatterns,1));

    LL = zeros(length(targetData),1);
    loggamma = cell(length(targetData),1);
	viterbi = cell(length(targetData),1);
    
    if (copySegments || ~isempty(testingSegments))
        % If we're not refitting on test data, just evaluate segment fit
        for i = 1:length(targetData)
            B = exp(ComputeLogB(targetData{i}, meanPatterns, iterationVar));
            if (copySegments)
                LL(i) = sum(log(sum(B.*trainingSegments{i},2)));
            else
                LL(i) = sum(log(sum(B.*testingSegments{i},2)));
            end
        end
    else
        % Fit segmentation based on mean patterns
        for i = 1:length(targetData)
            if (scrambleCorrespond)
                logB = ComputeLogB(targetData{i}, meanPatterns(:,scram(i,:)), iterationVar);
            else
                logB = ComputeLogB(targetData{i}, meanPatterns, iterationVar);
            end
            [loggamma{i}, viterbi{i}, LL(i)] = forward_backward_log(logB, Pi, EndPi, P);
        end
    end
    
    if (scrambleCorrespond)
        for i = 1:nTrainSets
            loggamma{i}(:,scram(i,:)) = loggamma{i};
        end
    end

    % Break after finding maximum log-likelihood
    ll_hist = [ll_hist; LL'];
    if (mean(LL) > mean(end_ll))
        end_ll = LL;
        end_scale = iterationVar;
        end_loggamma = loggamma;
        end_viterbi = viterbi;
    end
    if (mean(end_ll)-mean(LL) > 1)
        % LL has started to decrease
        break;
    end
    
    % Update segmentation for next round of BW
    if (maxsteps > 1 && ~lockTrainingSegment)
        trainingSegments = cellfun(@exp, loggamma, 'UniformOutput', false);
    end
    stepnum = stepnum + 1;
end

%% Output reformatting
start_ll = ll_hist(1,:)';
if (length(end_loggamma)==1)
    end_loggamma = end_loggamma{1};
    end_viterbi = end_viterbi{1};
end

end
