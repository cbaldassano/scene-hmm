function NormalViolin(Y)
% A gaussian is fit to each column of Y and plotted

hold on;
for i = 1:size(Y,2)
    mu = mean(Y(:,i));
    st = std(Y(:,i));
    pts = (mu-3*st):(st/50):(mu+3*st);
    distvals = normpdf(pts,mu,st);
    distvals = distvals/max(distvals)*0.25;
    fill([distvals distvals(1)]+i,[pts pts(1)],[0.6 0.6 0.6],'EdgeColor','none');
end
hold off;

end