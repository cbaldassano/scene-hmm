function PlotLOO_Janice()

%% Within modality
% nScenes = 2:20:282;
% meancorr = zeros(length(nScenes),17,101);
% test_LL = zeros(length(nScenes),17,101);
% for s = nScenes
%     for l = 1:17
%         loaded = load(fullfile('TO_LOO_withinModality_Janice',[num2str(l) '_' num2str(s)]));
%         meancorr((s-2)/20+1,l,:,:,:) = loaded.meancorr;
%         test_LL((s-2)/20+1,l,:,:,:) = loaded.test_LL;
%     end
% end
% meancorr = squeeze(mean(meancorr,2));
% test_LL = squeeze(mean(test_LL,2));
% 
% f1 = figure;
% f2 = figure;
%  
% figure(f1);
% shadedErrorBar(nScenes, mean(meancorr(:,2:end),2), [(nullconf(meancorr(:,2:end),0.01)-mean(meancorr(:,2:end),2))';mean(meancorr(:,2:end),2)']);
% hold on;
% plot(nScenes, meancorr(:,1));
% xlabel('Scenes');
% title('Janice within movie correlation');
% 
% figure(f2);
% plot(nScenes, meancorr(:,1)-mean(meancorr(:,2:end),2));
% xlabel('Scenes');
% title(['Janice within movie correlation increase']);
% 
% % figure(f2);
% % plot(nScenes, test_LL(:,1)-mean(test_LL(:,2:end),2), nScenes, nullconf(test_LL(:,2:end))-mean(test_LL(:,2:end),2));
% % legend('Log-likelihood vs null','p=0.01 null');
% % xlabel('Scenes');

%% Across modality
nScenes = [2:10:52];% 62:20:142];
meancorr = zeros(length(nScenes),17,101);
test_LL = zeros(length(nScenes),17,101);
for s = 1:length(nScenes)
    for l = 1:17
        loaded = load(fullfile('TO_LOO_acrossModality_Janice',[num2str(l) '_' num2str(nScenes(s))]));
        meancorr(s,l,:,:,:) = loaded.meancorr;
        test_LL(s,l,:,:,:) = loaded.test_LL;
    end
end
meancorr = squeeze(mean(meancorr,2));
test_LL = squeeze(mean(test_LL,2));

f1 = figure;
% f2 = figure;
 
% figure(f1);
% subplot(1,2,1);
plot(nScenes, test_LL(:,1)-mean(test_LL(:,2:end),2), nScenes, nullconf(test_LL(:,2:end),0.05)-mean(test_LL(:,2:end),2));
xlabel('Scenes');
title('Movie to recall log-likelihood vs. null');
curry = get(gca,'Ylim');
ylim([0 curry(2)]);

% subplot(1,2,2);
% plot(nScenes, meancorr(:,1)-mean(meancorr(:,2:end),2), nScenes, nullconf(meancorr(:,2:end),0.05)-mean(meancorr(:,2:end),2));
% xlabel('Scenes');
% title('Movie to recall correlation vs. null');
% curry = get(gca,'Ylim');
% ylim([0 curry(2)]);

% figure(f1);
% shadedErrorBar(nScenes, mean(meancorr(:,2:end),2), [(nullconf(meancorr(:,2:end),0.05)-mean(meancorr(:,2:end),2))';mean(meancorr(:,2:end),2)']);
% hold on;
% plot(nScenes, meancorr(:,1));
% xlabel('Scenes');
% title('Janice movie to recall correlation');

% figure(f2);
% plot(nScenes, test_LL(:,1)-mean(test_LL(:,2:end),2), nScenes, nullconf(test_LL(:,2:end))-mean(test_LL(:,2:end),2));
% legend('Log-likelihood vs null','p=0.01 null');
% xlabel('Scenes');

end

function t = nullconf(x,p,tail)
if (nargin < 2)
    p = 0.01;
end
if (nargin < 3)
    tail = 'right';
end
sorted = sort(x,2,'ascend');
if (strcmp(tail,'right'))
    t = sorted(:,round(size(sorted,2)*(1-p)));
elseif (strcmp(tail,'left'))
    t = sorted(:,round(size(sorted,2)*p));
end
end