function RAcorr = AnnotRegress(scene_entropy, diffs, diff_titles)
w=10;
conv_diffs = zeros(size(diffs));
for r = 1:size(diffs,1)
    conv_diffs(r,:) = conv(double(diffs(r,:)),ones(1,w),'same');
end
conv_diffs = double(conv_diffs>0);

RAcorr = corr2(scene_entropy, conv_diffs(1,:));
return;

regressors = conv_diffs;
regressor_titles = diff_titles;
% for i = 1:size(conv_diffs,1)
%     for j = (i+1):size(conv_diffs,1)
% %         regressors = [regressors;conv_diffs(i,:).*conv_diffs(j,:)];
% %         regressor_titles = [regressor_titles {[diff_titles{i} 'x' diff_titles{j}]}];
% %         regressors = [regressors;double((conv_diffs(i,:)+conv_diffs(j,:))>0)];
% %         regressor_titles = [regressor_titles {[diff_titles{i} '|' diff_titles{j}]}];
%     end
% end

% % Multiple regression
% regressors = [regressors;ones(1,1976)];
% regressor_titles = [regressor_titles {'c'}];
% [b,bint,~,~,stats] = regress(mean(scene_entropy)',regressors');
% for i = 1:length(b)
%     if (bint(i,1)*bint(i,2) > 0)
%         disp([regressor_titles{i} ': b=' num2str(b(i))]);
%     end
% end
% 
% % Nonnegative multiple regression
% regressors = [regressors;ones(1,1976)];
% regressor_titles = [regressor_titles {'c'}];
% b = lsqnonneg(regressors',mean(scene_entropy)');
% for i = 1:length(b)
%     if (b(i) > 0)
%         disp([regressor_titles{i} ': b=' num2str(b(i))]);
%     end
% end

% Sparse lasso regression
addpath('SpaSM_toolbox\');
[b, info] = lasso(normalize(regressors'),center(scene_entropy'));
lambda = find(all(b>=0),1,'last');
[b, sortinds] = sort(b(:,lambda),'descend');
for i = 1:length(b)
    if (b(i) > 0)
        disp([regressor_titles{sortinds(i)} ': b=' num2str(b(i))]);
    end
end
end