function SimulData()
rng(1);

V = 10;
K = 10;
T = 500;

chance = 0;
for p = 1:1000
    randsamp = randperm(T-1,K-1);
    chance = chance + (1/1000)*sum(randsamp<=(K-1))/(K-1);
end

% reps = 100;
% length_std = [0 0.25];
% noise_std = 1.75:0.25:3;
% 
% bound_match = zeros(length(noise_std), length(length_std), reps);
% 
% parfor j = 1:length(noise_std)
%     disp(['   ' num2str(j) '/' num2str(length(noise_std))]);
%     bound_match_j = zeros(length(length_std), reps);
%     for i = 1:length(length_std)
%         disp([num2str(i) '/' num2str(length(length_std))]);
% 
%         for k = 1:reps
%             event_means = randn(V,K);
% 
%             event_label = zeros(1,T);
%             start_TR = 1;
%             for e = 1:(K-1)
%                 event_length = round(((T-start_TR+1)/(K-e+1))*(1+ + length_std(i)*randn(1)));
%                 event_length = min(max(event_length,1),T-start_TR+1-(K-e+1));
%                 event_label(start_TR:(start_TR+event_length-1)) = e;
%                 start_TR = start_TR+event_length;
%             end
%             event_label(start_TR:end) = K;
% 
%             simul_data = zeros(V,T);
%             for t = 1:T
%                 simul_data(:,t) = noise_std(j)*randn(V,1)+event_means(:,event_label(t));
%             end
%             [~,~,~, loggamma]  = HMM_BW('trainingData',simul_data,'nScenes',K);
%             [~,scenelabel] = max(loggamma,[],2);
%             bound_match_j(i,k) = sum(diff(event_label)' & diff(scenelabel))/(K-1);
%         end
%         bound_match(j,:,:) = bound_match_j;
%     end
% end
% bound_match = permute(bound_match, [2 1 3]);
% save('Outputs\simuldata_highnoise','bound_match','length_std','noise_std');

% shadedErrorBar(noise_std,mean(bound_match(1,:,:),3),fliplr(abs(bsxfun(@minus, EmpiricalConf(squeeze(bound_match(1,:,:))), mean(bound_match(1,:,:),3)'))));
% hold on;
% shadedErrorBar(noise_std,mean(bound_match(2,:,:),3),fliplr(abs(bsxfun(@minus, EmpiricalConf(squeeze(bound_match(2,:,:))), mean(bound_match(2,:,:),3)'))));

low = load('Outputs\simuldata');
high = load('Outputs\simuldata_highnoise');
bound_match = cat(2, low.bound_match, high.bound_match);
length_std = low.length_std;
noise_std = [low.noise_std high.noise_std];

figure('Color',[1 1 1],'Position',[680         558        1047         420]);
pal = PTPalette(2);
maxp = HistViolin(noise_std, squeeze(bound_match(1,:,:))',(-1/9:1/9:1)+1/18, pal(1,:));
hold on;
HistViolin(noise_std, squeeze(bound_match(2,:,:))',(-1/9:1/9:1)+1/18, pal(2,:), maxp);
line([noise_std(1) noise_std(end)],chance*[1 1]);
xlabel('Noise level');
ylabel('Fraction Boundaries recovered');
set(gca,'XTick',noise_std);

figure('Color',[1 1 1],'Position',[520         300        1389         132]);
pal = PTPalette(12);
pal = pal(3:end,:);
uniform_im = zeros(1,T,3);
variable_im = zeros(1,T,3);
for t = 1:T
    uniform_im(1,t,:) = pal(ceil(t/T*K),:);
    variable_im(1,t,:) = pal(high.event_label(t),:);
end
subplot(2,1,1);
image(repmat(uniform_im,10,1,1));
axis off
subplot(2,1,2);
image(repmat(variable_im,10,1,1));
axis off
