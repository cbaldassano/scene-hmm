function r = findrows(subset, fullset)
% Find indices r such that fullset(r,:) = subset
% Assumes all rows in subset occur in fullset

[C,D] = sortrows([fullset;subset]);
E = all(C(1:end-1,:)==C(2:end,:),2);
F = D(E);
[~,X] = sort(D([false;E]));
r = F(X);
end