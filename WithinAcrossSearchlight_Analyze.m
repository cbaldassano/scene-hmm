function WithinAcrossSearchlight_Analyze()

load(fullfile('Janice','Movie','ISC25.mat'),'coords');

nv = size(coords,1);
nPerm = 1000;
nSubj = 17;
scenes = 10:10:120;

SLcount = zeros(nv,1);
WvsA = zeros(nv,length(scenes),nPerm+1);
max_scene_i = zeros(nv,nSubj);

disp('Loading results...');
for loo = 1:nSubj
    disp(['   ' num2str(loo) '/' num2str(nSubj)]);
    within_m_across = zeros(nv,length(scenes),nPerm+1);
    for z = 9:3:48
        disp(['      ' num2str(z) '/48']);
        slice = load(fullfile('TO_WAS',[num2str(z) '_' num2str(loo) '_10.mat']));
        slice_within_m_across = zeros(length(slice.allSLinds),length(scenes),nPerm+1);
        for scene_i = 1:length(scenes)
            nScenes = scenes(scene_i);
            scene_slice = load(fullfile('TO_WAS',[num2str(z) '_' num2str(loo) '_' num2str(nScenes) '.mat']));
            slice_within_m_across(:,scene_i,:) = scene_slice.within-scene_slice.across;            
        end
        for i = 1:length(slice.allSLinds)
            within_m_across(slice.allSLinds{i},:,:) = within_m_across(slice.allSLinds{i},:,:) + repmat(slice_within_m_across(i,:,:),length(slice.allSLinds{i}),1,1);
            if (loo == 1)
                SLcount(slice.allSLinds{i}) = SLcount(slice.allSLinds{i}) + 1;
            end
        end
    end
    within_m_across = bsxfun(@times, within_m_across, 1./SLcount);

    WvsA = WvsA + (1/nSubj)*within_m_across;
    [~,max_scene_i(:,loo)] = max(within_m_across(:,:,1),[],2);
end

% Within vs. across
max_scene = 10*mean(max_scene_i,2);
WvsA_max = squeeze(max(WvsA,[],2));
zWvsA = NullZscore(WvsA_max(:,1),WvsA_max(:,2:end));
p_WvsA = (1-normcdf(zWvsA))*8;
p_WvsA(isnan(p_WvsA)) = 1;
p_WvsA(p_WvsA>1) = 1;
q_WvsA = FDR(p_WvsA');

save(fullfile('Outputs','WAS_results'),'max_scene','max_scene_i','WvsA','WvsA_max','-v7.3');

nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
nMaps = 2;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = [WvsA_max(v,1) -log10(q_WvsA(v))];
end
save_nii(nii_template, 'Outputs/WvsA_max.nii.gz');

nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
nMaps = 1;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = max_scene(v);
end
save_nii(nii_template, 'Outputs/max_scene.nii.gz');

end

