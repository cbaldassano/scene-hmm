function boundary_bool = Loggamma2Boundary(loggamma)
H = -exp(loggamma).*loggamma;
H(isnan(H)) = 0;
scene_entropy = sum(H,2);

[~,peak_locs] = findpeaks(scene_entropy, 'Npeaks', size(loggamma,2),  'SortStr', 'descend');
boundary_bool = false(size(loggamma,1),1);
boundary_bool(peak_locs) = true;

end