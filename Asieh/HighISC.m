conditions = {'SherlockMovie','SherlockAudio2','SherlockAudio1'};%, ...
              %'MerlinMovie','MerlinAudio2','MerlinAudio1'};

nv = 56039;
ISC = zeros(nv,length(conditions));
for c = 1:length(conditions)
    load(fullfile(conditions{c},'wholebrain'),'allsubj');
    for s = 1:17
        group = mean(allsubj(:,:,1:17 ~= s),3);
        ISC(:,c) = ISC(:,c)+(1/17)*corr_columns(allsubj(:,:,s)',group')';
    end
end

highISC = ISC(:,1)>0.1 & ISC(:,2)>0.1 & ISC(:,3)>0.1;
load(fullfile(conditions{c},'wholebrain'),'coords','validvox');

validvox(validvox==true) = highISC;
coords = coords(highISC,:);

for c = 1:length(conditions)
    load(fullfile(conditions{c},'wholebrain_group'),'groupmean');
    groupmean = groupmean(highISC,:);
    save(fullfile(conditions{c},'highISC_group'),'groupmean','coords','validvox');
end