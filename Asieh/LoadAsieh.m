addpath('Z:\Asieh\ListenerProject\analysis');
% Watched Sherlock
names_sherlock = {'HM_071014','ML_072314','PL_052314','NZ_090814','AS_091914','NH_100314','JC_110714','JZ_111214','JH_112514','YP_120814',...
           'MW_111914','JS_011215','VR_012015','TP_021815','BT_021115','EE_030715','RS_030715'};
% Watched Merlin
names_merlin = {'GD_071514','MP_061514','AL_090214','HS_091314','AC_100414','RH_110114','HK_110114',...
            'JW_110314','ER_111314','JL_120814','JS_120914','RW_021115','RG_021115','AC_030415','RP_031415','EK_031415','TB_031715'};

conditions = {'SherlockMovie','SherlockAudio2','SherlockAudio1'};%, ...
              %'MerlinMovie','MerlinAudio2','MerlinAudio1'};
crops = cell(length(conditions),1);
for c = 1:length(crops)
    crops{c} = get5_info_listener('crop_special',conditions{c});
end

all_keptvox = zeros(271633,length(conditions),17);
for c = 1:length(conditions)
    for i = 1:17
        if (ismember(c,[1 2 6]))
            name = names_sherlock{i};
        else
            name = names_merlin{i};
        end
        load(['Z:\Asieh\ListenerProject\analysis\subjects\' name '\' conditions{c} '\trans_filtered_func_data.mat'], 'keptvox');
        all_keptvox(:,c,i) = keptvox;
    end
    validvox = mean(mean(all_keptvox,2),3)>=0.70;
end
    
for c = 1:length(conditions)
    disp(conditions{c});
%     A1 = [];
%     PMC = [];
    if (c == 1)
        T = 941;
    elseif (c == 2 || c == 3)
        T = 715;
    elseif (c == 4)
        T = 999;
    else
        T = 605;
    end
    allsubj = zeros(sum(validvox),T,17,'single');
    for i = 1:17
        if (ismember(c,[1 2 6]))
            name = names_sherlock{i};
        else
            name = names_merlin{i};
        end
        disp(['   ' name]);
        load(['Z:\Asieh\ListenerProject\analysis\subjects\' name '\' conditions{c} '\trans_filtered_func_data.mat']);
        opts.crop_special = crops{c};
        subj_data = zscore(crop_roitc(i,data,opts)')';
        subj_data(~keptvox,:) = NaN;
        allsubj(:,:,i) = subj_data(validvox,:);
        
%         load(['Z:\Asieh\ListenerProject\analysis\subjects\' name '\' conditions{c} '\auditory_cortex_trans_filtered_func_data.mat']);
%         opts.crop_special = crops{c};
%         subj_data = crop_roitc(i,rdata,opts);
%         A1 = cat(3,A1,zscore(subj_data,0,2));
%         
%         load(['Z:\Asieh\ListenerProject\analysis\subjects\' name '\' conditions{c} '\PMC_3mm_trans_filtered_func_data.mat']);
%         opts.crop_special = crops{c};
%         subj_data = crop_roitc(i,rdata,opts);
%         PMC = cat(3,PMC,zscore(subj_data,0,2));
    end
    
    coords = zeros(datasize(1),datasize(2),datasize(3),3);
    for x = 1:datasize(1)
        for y = 1:datasize(2)
            for z = 1:datasize(3)
                coords(x,y,z,:) = [x y z];
            end
        end
    end
    coords = reshape(coords, prod(datasize(1:3)), 3);
    coords = coords(validvox,:);
    
    groupmean = mean(allsubj,3,'omitnan');
    
    save(['Asieh\' conditions{c} '\wholebrain.mat'],'validvox','coords','allsubj','-v7.3');
    save(['Asieh\' conditions{c} '\wholebrain_group.mat'],'validvox','coords','groupmean');
    
%     save(['Asieh\' conditions{c} '\A1.mat'],'A1');
%     save(['Asieh\' conditions{c} '\PMC.mat'],'PMC');
% %     
%     rng(1);
%     for r = 1:20
%         subjorder = randperm(17);
%         A1_train = mean(A1(:,:,subjorder(1:9)),3);
%         A1_test = mean(A1(:,:,subjorder(10:end)),3);
%         save(['Asieh\' conditions{c} '\Resamples\A1_' num2str(r) '.mat'],'A1_train','A1_test');
%         
%         PMC_train = mean(PMC(:,:,subjorder(1:9)),3);
%         PMC_test = mean(PMC(:,:,subjorder(10:end)),3);
%         save(['Asieh\' conditions{c} '\Resamples\PMC_' num2str(r) '.mat'],'PMC_train','PMC_test');
%     end
end