addpath('Z:\Asieh\ListenerProject\analysis');
% Watched Sherlock
names_sherlock = {'HM_071014','ML_072314','PL_052314','NZ_090814','AS_091914','NH_100314','JC_110714','JZ_111214','JH_112514','YP_120814',...
           'MW_111914','JS_011215','VR_012015','TP_021815','BT_021115','EE_030715','RS_030715'};
% Watched Merlin
names_merlin = {'GD_071514','MP_061514','AL_090214','HS_091314','AC_100414','RH_110114','HK_110114',...
            'JW_110314','ER_111314','JL_120814','JS_120914','RW_021115','RG_021115','AC_030415','RP_031415','EK_031415','TB_031715'};

conditions = {'SherlockMovie','SherlockAudio2','SherlockAudio1'};%, ...
              %'MerlinMovie','MerlinAudio2','MerlinAudio1'};
crops = cell(length(conditions),1);
for c = 1:length(crops)
    crops{c} = get5_info_listener('crop_special',conditions{c});
end

for c = 1:length(conditions)
    disp(conditions{c});
    if (c == 1)
        T = 941;
    elseif (c == 2 || c == 3)
        T = 715;
    elseif (c == 4)
        T = 999;
    else
        T = 605;
    end
    allsubj = zeros(468,T,17,'single');
    for i = 1:17
        if (ismember(c,[1 2 6]))
            name = names_sherlock{i};
        else
            name = names_merlin{i};
        end
        disp(['   ' name]);
        load(['Z:\Asieh\ListenerProject\analysis\subjects\' name '\' conditions{c} '\BHipp_3mm_thr30_trans_filtered_func_data.mat']);
        opts.crop_special = crops{c};
        allsubj(:,:,i) = zscore(crop_roitc(i,rdata,opts)')';
    end
    
    save(['Asieh\' conditions{c} '\hipp.mat'],'allsubj');
end