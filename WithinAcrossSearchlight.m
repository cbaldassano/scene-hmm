function WithinAcrossSearchlight(job_id) %z_slice, loo, nScenes)
% if (ischar(z_slice))
%     z_slice = str2double(z_slice);
%     loo = str2double(loo);
%     nScenes = str2double(nScenes);
% end
if (ischar(job_id))
    job_id = str2double(job_id);
end

[z_slice, loo, nScenes] = ind2sub([14 17 8], job_id);
z_slice = z_slice*3+6;
nScenes = nScenes*10+80;
if (exist(fullfile('TO_WAS',[num2str(z_slice) '_' num2str(loo) '_' num2str(nScenes) '.mat']),'file'))
    return;
end

disp(['Producing ' num2str(z_slice) '_' num2str(loo) '_' num2str(nScenes) '.mat']);

load(fullfile('Janice','Movie','ISC25_loo',num2str(loo)));
load(fullfile('Janice','Movie','ISC25.mat'),'coords');

radius = 3;
step = 3;
w = 5; % TR difference for window
nPerm = 1000;

rng(1);
perms = zeros(nPerm, nScenes);
for i = 1:nPerm
    perms(i,:) = randperm(nScenes);
end

% Output vars
allSLinds = {};
within = zeros(0,nPerm+1);
across = zeros(0,nPerm+1);
entropy = zeros(0,size(group_train,2));

coord_dict = zeros(max(coords(:,1))+10,max(coords(:,2))+10,max(coords(:,3))+10);
coord_dict(sub2ind(size(coord_dict),coords(:,1),coords(:,2),coords(:,3))) = 1:size(coords,1);

tStart = tic;
xcenters = (1+radius):step:(max(coords(:,1))-radius);
ycenters = (1+radius):step:(max(coords(:,2))-radius);
for xcenter = xcenters
    for ycenter = ycenters
        SLinds = reshape(coord_dict(xcenter+(-radius:radius), ycenter+(-radius:radius), z_slice+(-radius:radius)),1,[]);
        if (sum(SLinds>0) <= 1)
            continue;
        end
        allSLinds{end+1} = SLinds(SLinds>0);

        [~, ~, ~, loggamma] = HMM_BW('trainingData',group_train(allSLinds{end},:),'nScenes',nScenes);
        [~,scenelabel] = max(loggamma,[],2);
        t = tabulate(scenelabel);
        scene_lengths = t(:,2);
        
        H = -exp(loggamma).*loggamma;
        H(isnan(H)) = 0;
        entropy(end+1,:) = sum(H,2);
        
        within(end+1,:) = zeros(1,nPerm+1);
        across(end+1,:) = zeros(1,nPerm+1);
        windiffs = zeros(length(scenelabel)-w,1);
        for t = 1:(length(scenelabel)-w)
            windiffs(t) = corr2(loo_test(allSLinds{end},t),loo_test(allSLinds{end},t+w));
        end
        for i = 1:(nPerm+1)
            same_scene = scenelabel(1:(length(scenelabel)-w)) == scenelabel((1+w):end);
            within(end,i) = mean(windiffs(same_scene));
            across(end,i) = mean(windiffs(~same_scene));
            
            if (i <= nPerm)
                % Permute boundaries, preserving scene length distribution
                scene_perm = scene_lengths(perms(i,:));
                scenelabel = repelem(1:nScenes,scene_perm);
            end
        end

        tElapsed = toc(tStart);
        fracComplete = ((xcenter-xcenters(1))/step*length(ycenters) + (ycenter-ycenters(1))/step)/(length(xcenters)*length(ycenters));
        disp([num2str(xcenter) ',' num2str(ycenter) ',' num2str(z_slice) ': ' num2str(length(allSLinds{end})) ' vox, ETA=' num2str(tElapsed*(1-fracComplete)/fracComplete/60) ' mins']);
    end
end

save(fullfile('TO_WAS',[num2str(z_slice) '_' num2str(loo) '_' num2str(nScenes) '.mat']), 'allSLinds', 'within', 'across', 'entropy');
end