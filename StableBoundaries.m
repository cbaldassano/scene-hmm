function peaklocs = StableBoundaries(scene_entropy, human_seg)

nReg = size(human_seg,1);

nullpeaks = zeros(1000,100);
nullcorr = zeros(1000,25,nReg);
rng(1);
for i = 1:1000
    scram = mean(phaseScramble(scene_entropy));
    peaks = sort(findpeaks(scram),'descend');
    if (length(peaks)<100)
        peaks = [peaks zeros(1,100-length(peaks))];
    end
    nullpeaks(i,:) = peaks(1:100);
    
    for w = 1:size(nullcorr,2)
        for r = 1:nReg
            nullcorr(i,w,r) = corr2(scram,conv(double(human_seg(r,:)),ones(1,w),'same'));
        end
    end
end
nullpeaks = nullpeaks(:);

peaks = sort(findpeaks(mean(scene_entropy)),'descend');
qvals = ones(length(peaks),1);
for i = 1:length(peaks)
    qvals(i) = 1-i/(i+sum(nullpeaks>peaks(i))/1000);
    if (qvals(i) > 0.05)
        break;
    end
end

thresh = peaks(find(qvals<0.05,1,'last'));

figure;
plot(mean(scene_entropy));
xlabel('Timepoints');
ylabel('Scene entropy');
for i = find(mean(scene_entropy) >= thresh)
    h = line([i i],[0 max(mean(scene_entropy))]);
    set(h,'Color','k','LineStyle','--','LineWidth',2);
end
hold on;
plot(human_seg*max(mean(scene_entropy)));

[peaks, peaklocs] = findpeaks(mean(scene_entropy));
peaklocs(peaks<thresh) = [];

truecorr = zeros(size(nullcorr,2),nReg);
for w = 1:size(nullcorr,2)
    for r = 1:nReg
        truecorr(w,r) = corr2(mean(scene_entropy),conv(double(human_seg(r,:)),ones(1,w),'same'));
    end
end
figure;
h = shadedErrorBar([],mean(mean(nullcorr,1),3),mean(std(nullcorr,[],1),3)*1.96);
set(get(get(h.mainLine,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(h.patch,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(h.edge(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(h.edge(2),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
hold on;
plot(truecorr);
curry = get(gca,'Ylim');
ylim([0 curry(2)]);
end