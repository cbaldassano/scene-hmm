function GroupAcrossModality()

nScram = 100;
nScenes = 2:5:47;

experiment = 'Asieh';
datasets = {'SherlockAudio1' 'SherlockAudio2'};
rois = {'A1' 'PMC'};

test_LL = zeros(length(nScenes),nScram+1,length(datasets),length(rois));
meancorr = zeros(length(nScenes),nScram+1,length(datasets),length(rois));
for dataset_ind = 1%1:length(datasets)
    disp(datasets{dataset_ind});
    for roi_ind = 2%1:length(rois)
        disp(['   ' rois{roi_ind}]);
        loaded = load(fullfile(experiment,'SherlockMovie',[rois{roi_ind} '.mat']));
        movie = mean(loaded.(rois{roi_ind}),3);
        loaded = load(fullfile(experiment,'SherlockMovie','segment'));
        movie_seg = loaded.Sherlock_movie_segment;
        T = size(movie,2);

        loaded = load(fullfile(experiment,datasets{dataset_ind},[rois{roi_ind} '.mat']));
        audio = mean(loaded.(rois{roi_ind}),3);
        loaded = load(fullfile(experiment,datasets{dataset_ind},'segment'));
        audio_seg = loaded.Sherlock_audio_segment;
        clear loaded;
        
        for s = 5%1:length(nScenes) %(length(nScenes)+1)
            disp(['      ' num2str(s)]);
            if s == (length(nScenes)+1)
                % First is real, rest are scrambles
                gammas = zeros(T,22,nScram+1);
                gammas(:,:,1) = LabelToGamma(movie_seg);
                %[~, ~, end_scale, loggamma] = HMM_BW('trainingData',movie,'trainingSegments',movie_seg,'lockTrainingSegment',true,'PCAdims',-1);
            else
                % First is real, rest are scrambles
                gammas = zeros(T,nScenes(s),nScram+1);
                % Learn (soft) segmentation on movie
                [~, ~, ~, loggamma] = HMM_BW('trainingData',movie,'nScenes',nScenes(s),'PCAdims',-1);
                gammas(:,:,1) = exp(loggamma);
            end
            
            rng(1);
            for i = 1:nScram
                gammas(:,:,i+1) = gammas(:,randperm(size(gammas,2)),1);
            end
        
            for i = 1:(nScram+1)
                sceneV = mean(SceneVariance(movie,gammas(:,:,i)));
                [test_LL(s,i,dataset_ind,roi_ind), ~, ~, loggamma, viterbi] = ...
                    HMM_BW('trainingData',movie,'trainingSegments',gammas(:,:,i),'sceneVariance',sceneV, ...%end_scale,...%
                                'testingData',audio,'PCAdims',-1);

                % Calculate scene correlations with group
                meanPatterns_group =  movie*bsxfun(@times,gammas(:,:,i),1./sum(gammas(:,:,i),1));
                meanPatterns_loo =  audio*bsxfun(@times,exp(loggamma),1./sum(exp(loggamma),1));
                meancorr(s,i,dataset_ind,roi_ind) = mean(corr_columns(meanPatterns_group,meanPatterns_loo));
            end
        end
        
    end
end
end
% plot(nScenes,NullZscore(test_LL(:,1,1,2),test_LL(:,2:end,1,2)))