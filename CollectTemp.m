function allvar = CollectTemp(dirname, varname)
    f = dir(fullfile(dirname,'*.mat'));
    first = load(fullfile(dirname,f(1).name),varname);
    
    allvar = zeros(length(f),numel(first.(varname)));
    for i = 1:length(f)
        loaded = load(fullfile(dirname,f(i).name),varname);
        allvar(i,:) = reshape(loaded.(varname),[],1);
    end
end