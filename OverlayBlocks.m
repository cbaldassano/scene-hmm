function OverlayBlocks(movie_segment, audio_segment)%, edge)%, movie_gamma, audio_gamma)


% seg_blocks = zeros(length(movie_segment), length(audio_segment));
% for i = 1:max(movie_segment)
%     seg_blocks(movie_segment==i, audio_segment==i) = 1;
% end
% image(repmat(1-seg_blocks,1,1,3));
% hold on;

% gammaprod = movie_gamma*audio_gamma';
% im2 = imagesc(1.5*(1:length(audio_segment)),1.5*(1:length(movie_segment)),gammaprod);
% set(im2,'AlphaData',sqrt(gammaprod*2));
% xlabel('Audio (seconds)'); ylabel('Video (seconds)');

hold on;
for i = setdiff(unique(movie_segment),0)'
    audio_start = find(audio_segment==i,1,'first');
    if (isempty(audio_start))
        continue;
    end
    end_TR = audio_start;
    while (end_TR <= length(audio_segment) && audio_segment(end_TR)==i)
        end_TR = end_TR+1;
    end
    end_TR = end_TR-1;
    
    rectpos = 1.5*...
             [audio_start-0.5, ...
             find(movie_segment==i,1,'first')-0.5,...
             end_TR-audio_start+1, ...
             find(movie_segment==i,1,'last') - find(movie_segment==i,1,'first')+1];
    rectangle('Position',rectpos,'EdgeColor','k','LineWidth',1);
    %rectangle('Position',rectpos+[-1 -1 2 2],'EdgeColor',edge);
end
hold off;

%set(gca,'YDir','normal');

end