function JaniceTrackRecall()
loaded = load(fullfile('Janice','Movie','PCC.mat'));
movie = mean(loaded.PCC,3);
clear loaded;

loaded = load(fullfile('Janice','Recall','PCC.mat'));
recall = loaded.PCC;


loaded = load(fullfile('Janice','recall_segments.mat'));
movie_seg = loaded.movie_TRs;
recall_seg = loaded.recall_TRs;
clear loaded;

nScenes = 22;
loggamma = cell(17,1);
for s = [1 10 17]
    [~, ~, ~, loggamma{s}] = HMM_BW('trainingData',{movie, recall{s}},'nScenes',nScenes);
%     subplot(3,6,s);
%     im = imagesc(1.5*(1:size(recall{s},2)),1.5*(1:size(movie,2)),exp(loggamma{1})*exp(loggamma{2})');
%     set(gca,'YDir','normal');
%     xlabel('Recall (sec)');
%     ylabel('Movie (sec)');
%     set(im,'AlphaData',sqrt(exp(loggamma{1})*exp(loggamma{2}')));
%     title(num2str(s));
%     OverlayBlocks(movie_seg{s}, recall_seg{s});
%     drawnow;
end

figure('Color',[1 1 1],'Position',[673         467        1060         631]);
colors = PTPalette(3);
subj = [1 10 17];
Tm = size(movie,2);
hold off
for s = [1 2 3]
    Tr = size(recall{subj(s)},2);
    track = exp(loggamma{subj(s)}{1})*exp(loggamma{subj(s)}{2})';
    im = image(1.5*(1:Tr),1.5*(1:Tm),...
        repmat(track,1,1,3).*permute(repmat(colors(s,:),Tm,1,Tr),[1 3 2]) + repmat(1-track,1,1,3).*permute(repmat([1 1 1],Tm,1,Tr),[1 3 2]));
    set(im,'AlphaData',sqrt(track*4));
    hold on;
end

% for s = [3 2 1]
%     Tr = size(recall{subj(s)},2);
%     track = exp(loggamma{subj(s)}{1})*exp(loggamma{subj(s)}{2})';
%     im = image(1.5*(1:Tr),1.5*(1:Tm),...
%         repmat(track,1,1,3).*permute(repmat(colors(s,:),Tm,1,Tr),[1 3 2]) + repmat(1-track,1,1,3).*permute(repmat([1 1 1],Tm,1,Tr),[1 3 2]));
%     set(im,'AlphaData',0.5);
% end

for s = [1 2 3]
    OverlayBlocks(movie_seg{subj(s)}, recall_seg{subj(s)},colors(s,:));
end
xlim([0 1.5*size(recall{subj(3)},2)])
xlabel('Recall (sec)');
ylabel('Movie (sec)');
set(gca,'YDir','normal');
end