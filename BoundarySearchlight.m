function BoundarySearchlight(z_slice, nScenes)
load(fullfile('Janice','Movie','group_ISC25.mat'));

radius = 3;
step = 3;

allSLinds = {};
fit_ll = [];
scene_entropy = zeros(0, size(groupdata,2));

coord_dict = zeros(max(coords(:,1))+10,max(coords(:,2))+10,max(coords(:,3))+10);
coord_dict(sub2ind(size(coord_dict),coords(:,1),coords(:,2),coords(:,3))) = 1:size(coords,1);

tStart = tic;
xcenters = (1+radius):step:(max(coords(:,1))-radius);
ycenters = (1+radius):step:(max(coords(:,2))-radius);
for xcenter = xcenters
    for ycenter = ycenters
        SLinds = [];
        for x = -radius:radius
            for y = -radius:radius
                for z = -radius:radius
                    SLinds = [SLinds coord_dict(xcenter+x, ycenter+y, str2double(z_slice)+z)];
                end
            end
        end
        if (sum(SLinds>0) <= 1)
            continue;
        end
        allSLinds{end+1} = SLinds(SLinds>0);

        [~, fit_ll(end+1), ~, loggamma] = HMM_BW('trainingData',groupdata(allSLinds{end},:),'nScenes',str2double(nScenes));
        H = -exp(loggamma).*loggamma;
        H(isnan(H)) = 0;
        scene_entropy(end+1,:) = sum(H,2);
        
        tElapsed = toc(tStart);
        fracComplete = ((xcenter-xcenters(1))/step*length(ycenters) + (ycenter-ycenters(1))/step)/(length(xcenters)*length(ycenters));
        disp([num2str(xcenter) ',' num2str(ycenter) ',' num2str(z_slice) ': ' num2str(length(allSLinds{end})) ' vox, ETA=' num2str(tElapsed*(1-fracComplete)/fracComplete/60) ' mins']);
    end
end

save(fullfile('TO_BS',[z_slice '_' nScenes '.mat']), 'allSLinds', 'fit_ll', 'scene_entropy');
end