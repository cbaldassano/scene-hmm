function LOO_withinModality_withinAcross(loo, nScenes)

dirname = 'TO_LOO_withinModality_withinAcross';
loo = str2double(loo);
nScenes = str2double(nScenes);

experiment = 'Asieh';%'Janice';
datasets = {'SherlockMovie' 'SherlockAudio1' 'SherlockAudio2'};%{'Movie'};%
rois = {'A1' 'PMC'};%{'PCC'};%

wins = 1:40;
within = zeros(length(datasets),length(rois),length(wins));
across = zeros(length(datasets),length(rois),length(wins));
for dataset_ind = 1:length(datasets)
    for roi_ind = 1:length(rois)
        loaded = load(fullfile(experiment,datasets{dataset_ind},[rois{roi_ind} '.mat']));
        D = loaded.(rois{roi_ind});
        clear loaded;

        train_subj = setdiff(1:size(D,3),loo);
        group_train = mean(D(:,:,train_subj),3);
        T = size(D,2);

        % Learn (soft) segmentation on all but one subject
        [~, ~, ~, loggamma] = HMM_BW('trainingData',group_train,'nScenes',nScenes,'PCAdims',-1);
        [~,scenelabel] = max(loggamma,[],2);
        
        for w = wins
            within_count = 0;
            across_count = 0;
            for t = 1:(length(scenelabel)-w)
                if (scenelabel(t) == scenelabel(t+w))
                    within(dataset_ind,roi_ind,w) = within(dataset_ind,roi_ind,w) + corr2(D(:,t,loo),D(:,t+w,loo));
                    within_count = within_count + 1;
                else
                    across(dataset_ind,roi_ind,w) = across(dataset_ind,roi_ind,w) + corr2(D(:,t,loo),D(:,t+w,loo));
                    across_count = across_count + 1;
                end
            end
            within(dataset_ind,roi_ind,w) = within(dataset_ind,roi_ind,w)/within_count;
            across(dataset_ind,roi_ind,w) = across(dataset_ind,roi_ind,w)/across_count;
        end


        disp([datasets{dataset_ind} ' ' rois{roi_ind}]);
        if ~exist(dirname,'dir')
            mkdir(dirname);
        end
        filename = [num2str(loo) '_' num2str(nScenes) '.mat'];
        save([dirname '/' filename],'within','across');
    end
end

end