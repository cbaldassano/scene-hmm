function C = corr_columns(A,B)
% Computes the pairwise correlations between columns of A and B
% Equivalent to diag(corr(A,B)) but avoids calculating all correlations
An=bsxfun(@minus,A,mean(A,1));
Bn=bsxfun(@minus,B,mean(B,1));
An=bsxfun(@times,An,1./sqrt(sum(An.^2,1)));
Bn=bsxfun(@times,Bn,1./sqrt(sum(Bn.^2,1)));
C=sum(An.*Bn,1);

end