function LeadSearchlight_MaxScene(job_id)

if (ischar(job_id))
    job_id = str2double(job_id);
end

[z_slice, rngseed] = ind2sub([14 20], job_id);
z_slice = z_slice*3+6;
fname = [num2str(z_slice) '_' num2str(rngseed) '.mat'];
if (exist(fullfile('TO_LS',fname),'file'))
    return;
end

disp(['Producing ' fname]);

movie = load(fullfile('Asieh','SherlockMovie','highISC_group'));
audio1 = load(fullfile('Asieh','SherlockAudio1','highISC'));
audio2 = load(fullfile('Asieh','SherlockAudio2','highISC'));

nSubj = 17;
nScenes = 2:5:72;
nPerm = 5+(rngseed==1);

load(fullfile('TO_AS',[num2str(z_slice) '_2.mat']), 'allSLinds');
nSL = length(allSLinds);
loggamma = cell(nSL,nPerm);
tStart = tic;
for sl = 1:nSL
    sceneZ = zeros(length(nScenes),1);
    for ns = 1:length(nScenes)
        scene_data = load(fullfile('TO_AS',[num2str(z_slice) '_' num2str(nScenes(ns)) '.mat']));
        sceneZ(ns) = NullZscore(scene_data.test_LL(sl,1),scene_data.test_LL(sl,2:end));
    end
    
    [~,maxscene_i] = max(sceneZ);
    
    simuldata = cell(3,1);
    simuldata{1} = zscore(movie.groupmean(allSLinds{sl},:),0,2);
    rng(rngseed);
    for p = 1:nPerm
        if (rngseed==1 && p==1)
            simuldata{2} = zscore(mean(audio1.allsubj(allSLinds{sl},:,:),3),0,2);
            simuldata{3} = zscore(mean(audio2.allsubj(allSLinds{sl},:,:),3),0,2);
        else
            subj = 1:nSubj;
            n1a = randi([8 9]);
            group_a = {randperm(17,n1a), randperm(17,17-n1a)};
            group_b = {subj(~ismember(subj,group_a{1})), subj(~ismember(subj,group_a{2}))};
            simuldata{2} = zscore(mean(cat(3,audio1.allsubj(allSLinds{sl},:,group_a{1}),audio2.allsubj(allSLinds{sl},:,group_a{2})),3),0,2);
            simuldata{3} = zscore(mean(cat(3,audio1.allsubj(allSLinds{sl},:,group_b{1}),audio2.allsubj(allSLinds{sl},:,group_b{2})),3),0,2);
        end
        [~, ~, ~, loggamma{sl,p}] = HMM_BW('trainingData',simuldata,'nScenes',nScenes(maxscene_i));
    end
    tElapsed = toc(tStart);
    fracComplete = sl/length(allSLinds);
    disp([num2str(sl) '/' num2str(length(allSLinds)) ': ' num2str(length(allSLinds{sl})) ' vox, ETA=' num2str(tElapsed*(1-fracComplete)/fracComplete/60) ' mins']);
end

save(fullfile('TO_LS',fname), 'loggamma');
end