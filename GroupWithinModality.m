function GroupWithinModality()

nScenes = 22;

experiment = 'Asieh'; %'Janice';
datasets = {'SherlockMovie' 'SherlockAudio1' 'SherlockAudio2'}; %{'Movie'};%
rois = {'A1' 'PMC'}; %{'PCC'}; %

for dataset_ind = 1 %1:length(datasets)
    disp(datasets{dataset_ind});
    %hipp_data = load(fullfile(experiment,datasets{dataset_ind},'hipp.mat'));
    for roi_ind = 2 %1:length(rois)
        disp(['   ' rois{roi_ind}]);
        loaded = load(fullfile(experiment,datasets{dataset_ind},[rois{roi_ind} '.mat']));
        D = loaded.(rois{roi_ind});
        D = mean(D,3);
        clear loaded;
        
        for s = 1:length(nScenes) %(length(nScenes)+1)
                [~, ~, ~, loggamma] = HMM_BW('trainingData',D,'nScenes',nScenes(s));
                gammas(:,:,1) = exp(loggamma);
                
                H = -exp(loggamma).*loggamma;
                H(isnan(H)) = 0;
                scene_entropy = sum(H,2);
                
                [~, boundlocs] = findpeaks(scene_entropy, 'SortStr', 'descend', 'NPeaks', nScenes(s));
                boundary_bool = false(length(scene_entropy),1);
                boundary_bool(boundlocs) = true;
                
                hipp_data = load(fullfile(experiment,datasets{1},'hipp.mat'));
                z_peak = HippCorr(boundary_bool, hipp_data.allsubj)
        end
    end
end