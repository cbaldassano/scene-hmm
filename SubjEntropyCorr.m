function SubjEntropyCorr()

nScenes = 2:20:122;
nScram = 0;
ISC = zeros(length(nScenes),nScram+1,17);
for i = 1:length(nScenes)
    load(fullfile('TO_SceneEntropy',num2str(nScenes(i))));
    
    entropy = zeros(17,1976,nScram+1);
    entropy(:,:,1) = scene_entropy;
    
    rng(1);
    for s = 1:nScram
        entropy(:,:,s+1) = phaseScramble(scene_entropy);
    end
    
    for s = 1:nScram+1
        for j = 1:17
            ISC(i,s,j) = corr2(mean(entropy(1:17~=j,:,s),1),entropy(j,:,s));
        end
    end
end

shadedErrorBar(2:20:122,mean(ISC(:,1,:),3),std(ISC(:,1,:),[],3)/sqrt(17)*1.96)
end