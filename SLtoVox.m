function vox_data = SLtoVox(SLinds, data, nv)
% SLinds is Nx1 cell
% data is Nx(arbitrary)

SLcount = zeros(nv,1);
datadim = size(data);
vox_data = zeros([nv datadim(2:end)]);
for i = 1:length(SLinds)
    vox_data(SLinds{i},:) = vox_data(SLinds{i},:) + repmat(data(i,:),length(SLinds{i}),1);
    SLcount(SLinds{i}) = SLcount(SLinds{i}) + 1;
end
vox_data = bsxfun(@times, vox_data, 1./SLcount);

end