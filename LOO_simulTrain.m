function LOO_simulTrain(loo, nScenes)

nScram = 100;
dirname = 'TO_LOO_simulTrain';
loo = str2double(loo);
nScenes = str2double(nScenes);
tic;
datasets = {'SherlockAudio1' 'SherlockAudio2'};
rois = {'A1' 'PMC'};
test_LL = zeros(nScram+1,length(datasets),length(rois),2);
meancorr = zeros(nScram+1,length(datasets),length(rois),2);
for dataset_ind = 1:length(datasets)
    for roi_ind = 1:length(rois)
        loaded = load(fullfile('Asieh','SherlockMovie',[rois{roi_ind} '.mat']));
        movie = loaded.(rois{roi_ind});
        clear loaded;

        loaded = load(fullfile('Asieh',datasets{dataset_ind},[rois{roi_ind} '.mat']));
        audio = loaded.(rois{roi_ind});
        clear loaded;
        
        % Select voxels with good ISC in both modalities
        ISC_movie = zeros(1,size(movie,1));
        ISC_audio = zeros(1,size(audio,1));
        nSubj = size(movie,3);
        for i = 1:nSubj
            if (i == loo)
                continue;
            end
            group = mean(movie(:,:,~ismember(1:nSubj,[i,loo])),3);
            ISC_movie = ISC_movie + corr_columns(group',movie(:,:,i)')/(nSubj-1);
            group = mean(audio(:,:,~ismember(1:nSubj,[i,loo])),3);
            ISC_audio = ISC_audio + corr_columns(group',audio(:,:,i)')/(nSubj-1);
        end
        bothISC = ISC_movie.*ISC_audio;
        [~,ISCorder] = sort(bothISC,'descend');
        nonnan = find(~isnan(bothISC(ISCorder)),1,'first');
        nvox = 300;
        movie = movie(ISCorder(nonnan:(nonnan+nvox-1)),:,:);
        audio = audio(ISCorder(nonnan:(nonnan+nvox-1)),:,:);
        

        train_subj = setdiff(1:size(movie,3),loo);
        group_train = {mean(movie(:,:,train_subj),3) mean(audio(:,:,train_subj),3)};

        rng(1);
        for i = 1:(nScram+1)
            if (i == 1)
                [~, ~, ~, loggamma] = HMM_BW('trainingData',group_train,'nScenes',nScenes,'PCAdims',-1);
            else
                [~, ~, ~, loggamma] = HMM_BW('trainingData',group_train,'nScenes',nScenes,'PCAdims',-1,'scrambleCorrespond',true);
            end
            
            % Compute LL on held out subject
            sceneV = (SceneVariance(movie(:,:,train_subj),exp(loggamma{1}))+SceneVariance(audio(:,:,train_subj),exp(loggamma{2})))/2;
            test_LL(i,dataset_ind,roi_ind,:) = ...
                HMM_BW('trainingData',group_train,'trainingSegments',{exp(loggamma{1}) exp(loggamma{2})},'sceneVariance',sceneV, ...
                            'testingData', {movie(:,:,loo) audio(:,:,loo)},'copySegments',true,'PCAdims',-1);

            % Calculate scene correlations with group
            meanMoviePatterns_group =  mean(movie(:,:,train_subj),3)*bsxfun(@times,exp(loggamma{1}),1./sum(exp(loggamma{1}),1));
            meanMoviePatterns_loo =  movie(:,:,loo)*bsxfun(@times,exp(loggamma{1}),1./sum(exp(loggamma{1}),1));
            meancorr(i,dataset_ind,roi_ind,1) = mean(corr_columns(meanMoviePatterns_group,meanMoviePatterns_loo));
            meanAudioPatterns_group =  mean(audio(:,:,train_subj),3)*bsxfun(@times,exp(loggamma{2}),1./sum(exp(loggamma{2}),1));
            meanAudioPatterns_loo =  audio(:,:,loo)*bsxfun(@times,exp(loggamma{2}),1./sum(exp(loggamma{2}),1));
            meancorr(i,dataset_ind,roi_ind,2) = mean(corr_columns(meanAudioPatterns_group,meanAudioPatterns_loo));
        end

        disp([datasets{dataset_ind} ' ' rois{roi_ind} ' ' num2str(mean(test_LL(1,dataset_ind,roi_ind,:))) ' ' num2str(mean(meancorr(1,dataset_ind,roi_ind,:))) ' ' num2str(mean(mean(test_LL(2:end,dataset_ind,roi_ind,:)))) ' ' num2str(mean(mean(meancorr(2:end,dataset_ind,roi_ind,:))))]);
        toc;
        if ~exist(dirname,'dir')
            mkdir(dirname);
        end
        filename = [num2str(loo) '_' num2str(nScenes) '.mat'];
        save([dirname '/' filename],'test_LL','meancorr');
    end
end

end