function CharacterJSON(scene_characters)

scene_characters = scene_characters(cellfun(@(x) ~isempty(x), scene_characters));
writeJSON(scene_characters, 'allcharacter.json');

allchar = unique(vertcat(scene_characters{:}));
charcount = zeros(length(allchar),1);
for i = 1:length(scene_characters)
    for j = 1:length(scene_characters{i})
        charcount(cellfun(@(x) strcmp(x, scene_characters{i}{j}), allchar)) = ...
            charcount(cellfun(@(x) strcmp(x, scene_characters{i}{j}), allchar)) + 1;
    end
end
allchar = allchar(charcount > 2);
for i = 1:length(scene_characters)
    scene_characters{i} = scene_characters{i}(ismember(scene_characters{i}, allchar));
end

scene_characters = scene_characters(cellfun(@(x) ~isempty(x), scene_characters));
writeJSON(scene_characters, 'threescene.json');
end

function writeJSON(scene_characters, fname)
allchar = unique(vertcat(scene_characters{:}));
json.characters = struct('id',cellfun(@(x) strrep(x, ' ', '-'), allchar', 'UniformOutput', false), 'name', allchar', 'affiliation', repmat({'other'}, 1, length(allchar)));

json.scenes = cell(1,length(scene_characters));
for i = 1:length(scene_characters)
    json.scenes{i} = cellfun(@(x) strrep(x, ' ', '-'), scene_characters{i}', 'UniformOutput', false);
end

opt.FileName = fname;
savejson('',json,opt);
end