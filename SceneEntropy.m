function SceneEntropy(nScenes)

loaded = load(fullfile('Janice','Movie','PCC.mat'));
movie = loaded.PCC;
TM = size(movie,2);
clear loaded;

scene_entropy = zeros(17,TM);
for loi = 1:17
    disp(['Subj ' num2str(loi)]);
    
    % Learn (soft) segmentation in one subject
    [~, ~, ~, loggamma] = HMM_BW('trainingData',movie(:,:,loi),'nScenes',str2double(nScenes),'PCAdims',-1);
    H = -exp(loggamma).*loggamma;
    H(isnan(H)) = 0;
    scene_entropy(loi,:) = sum(H,2);
end

save(fullfile('TO_SceneEntropy',nScenes),'scene_entropy');
end