function smoothed_maps = GaussSmooth(coords, maps, sigma)
% coords=Nx3, maps = Nxany
smoothed_maps = zeros(size(maps));
disp('Smoothing...');
map_reshape = maps(:,:);
for v = 1:size(coords,1)
    if (mod(v,round(size(coords,1)/10))==1)
        disp(['   ' num2str(v) '/' num2str(size(coords,1))]);
    end
    w = mvnpdf(coords, coords(v,:), sigma*eye(size(coords,2)));
    smoothed_maps(v,:) = w'*map_reshape/sum(w);
end
end