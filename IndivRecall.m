function IndivRecall(nScenes)

nScram = 100;

loaded = load(fullfile('Janice','Movie','A1.mat'));%PCC.mat'));
movie = loaded.A1;%PCC;
clear loaded;

loaded = load(fullfile('Janice','Recall','A1.mat'));%'PCC.mat'));
recall = loaded.A1; %PCC;

loaded = load(fullfile('Janice','Movie','hipp.mat'));
movie_hipp = loaded.allsubj;

loaded = load(fullfile('Janice','Recall','hipp.mat'));
recall_hipp = loaded.hipp;

clear loaded;

% % First is real, rest are scrambles
% gammas = zeros(size(movie,2),nScenes,nScram+1);
% [~, ~, ~, loggamma] = HMM_BW('trainingData',mean(movie,3),'nScenes',nScenes);
% gammas(:,:,1) = exp(loggamma);
% 
% % Randomly reorder the segments
% rng(1);
% for i = 1:nScram
%     gammas(:,:,i+1) = gammas(:,randperm(nScenes),1);
% end
% 
% test_LL = zeros(nScram+1,17);
% boundary_bool = zeros(size(movie,2),17);
% for i = 1:(nScram+1)
%     for s = 1:17
%         sceneV = mean(SceneVariance(movie(:,:,1:17 ~= s),gammas(:,:,i)));
%         [test_LL(i,s), ~, ~, loggamma] = ...
%             HMM_BW('trainingData',mean(movie,3),'trainingSegments',gammas(:,:,i),'sceneVariance',sceneV, ...
%                         'testingData',recall{s});
%         if (i == 1)
%             H = -exp(loggamma).*loggamma;
%             H(isnan(H)) = 0;
%             recall_entropy = sum(H,2);
%             [~, subj_peaks] = findpeaks(recall_entropy, 'SortStr', 'descend', 'NPeaks', nScenes);
%             boundary_bool(subj_peaks,s) = 1;
%         end
%     end
% end

boundary_bool = cell(17,1);
for s = 1:17
    disp(num2str(s));
    [~, ~, ~, loggamma] = HMM_BW('trainingData',{mean(movie,3), recall{s}},'nScenes',nScenes);
    H = -exp(loggamma{2}).*loggamma{2};
    H(isnan(H)) = 0;
    recall_entropy = sum(H,2);
    [~, subj_peaks] = findpeaks(recall_entropy, 'SortStr', 'descend', 'NPeaks', nScenes);
    boundary_bool{s} = zeros(size(recall{s},2),1);
    boundary_bool{s}(subj_peaks) = 1;
end
[~,sh] = HippCorr(boundary_bool, recall_hipp);

% xlim([-17 17])
% curry = get(gca,'YLim')
% line(2.5*1.5*[1 1],curry)
% line(-2.5*1.5*[1 1],curry)
% xlabel('Seconds from scene boundary')
% ylabel('Mean hippocampal activity')
% hold on; errorbar([-10.5 0 10.5],mean([mean(sh(:,17:25),2) sh(:,28) mean(sh(:,31:39),2)]),std([mean(sh(:,17:25),2) sh(:,28) mean(sh(:,31:39),2)])/sqrt(17))
% [~,p] = ttest(sh(:,28)-mean(sh(:,[17:25 31:39]),2),0,0.05,'right')

% To convert recall segments: cellfun(@(x) [0;(diff(x)>0)], recall_TRs, 'UniformOutput', false)

% 
% H = -gammas(:,:,1).*log(gammas(:,:,1));
% H(isnan(H)) = 0;
% movie_entropy = sum(H,2);
% [~, movie_peaks] = findpeaks(movie_entropy, 'SortStr', 'descend', 'NPeaks', nScenes);
% boundary_bool = zeros(length(movie_entropy),1);
% boundary_bool(movie_peaks) = 1;
% HippCorr(boundary_bool, movie_hipp);


end