% Insets for timescales figure
% ROIs from high_WvsA_rois

CorrWithBounds(groupdata(lPCC2,1014:1121),3);
CorrWithBounds(groupdata(lVC,1014:1121),15); % Optimal #scenes about 4-5x greater
% Number of scenes determined by computing round(max(max_scene(ROI)), then
% seeing how many scenes occur in 1:400
%CorrWithBounds(groupdata(rVC,1:400),22); CorrWithBounds(groupdata(rAC,1:400),14); CorrWithBounds(groupdata(lPCC,1:400),10);


% Inset for hipp figure
% PGap_inds in Janice/ROIinds
hipp_movie = load(fullfile('Janice','Movie','hipp'));
hipp_movie = hipp_movie.hipp;

load(fullfile('Janice','Movie','ISC25.mat'),'coords');
nv = size(coords,1);
T = 1976;
SLinds = {};
entropy = zeros(0,T);
scenelabel = zeros(0,T);
for z = 9:3:48
    slice = load(fullfile('TO_WAS',[num2str(z) '.mat']));
    SLinds = [SLinds slice.allSLinds];
    entropy = [entropy; slice.entropy];
    scenelabel = [scenelabel; slice.scenelabel];
end

leftPGap = zeros(17,21);
leftCount = 0;
rightPGap = zeros(17,21);
rightCount = 0;
for i = 1:length(SLinds)
    if mean(ismember(SLinds{i},PGap_inds))<0.5
        continue;
    end
    [~,locs] = findpeaks(entropy(i,:),'NPeaks',max(scenelabel(i,:))-1,'SortStr','descend');
    boundlabel = zeros(T,1);
    boundlabel(locs) = 1;
    hipp_avg = HippCorr(boundlabel, hipp_movie);
    if (mean(coords(SLinds{i},1))<mean(coords(:,1)))
        leftPGap = leftPGap + hipp_avg;
        leftCount = leftCount+1;
    else
        rightPGap = rightPGap + hipp_avg;
        rightCount = rightCount+1;
    end
end
leftPGap = leftPGap/leftCount;
rightPGap = rightPGap/rightCount;

figure
shadedErrorBar(-10*1.5:1.5:10*1.5,mean(leftPGap),std(leftPGap)/sqrt(17)*tinv([0.9762],17-1));
curry = get(gca,'YLim');
h = line([0 0],curry);
set(h,'Color','k','LineStyle','--','LineWidth',2);
xlabel('Seconds from HMM boundary');
ylabel('Mean Hipp Activity');
title('Left PG');
box off;

figure
shadedErrorBar(-10*1.5:1.5:10*1.5,mean(rightPGap),std(rightPGap)/sqrt(17)*tinv([0.9762],17-1));
curry = get(gca,'YLim');
h = line([0 0],curry);
set(h,'Color','k','LineStyle','--','LineWidth',2);
xlabel('Seconds from HMM boundary');
ylabel('Mean Hipp Activity');
title('Right PG');
box off;



% Example bounds compared to humans
PGsl = 1762;
PCCsl = 1913;
[~,PGlocs] = findpeaks(entropy(PGsl,:),'NPeaks',max(scenelabel(PGsl,:))-1,'SortStr','descend');

[~,PCClocs] = findpeaks(entropy(PCCsl,:),'NPeaks',max(scenelabel(PCCsl,:))-1,'SortStr','descend');
pal = PTPalette(6);
allbounds = [{PCClocs};{PGlocs};bounds];
figure('Position',[145         304        1546         420]);
for i = 1:size(allbounds,1)
    for j = 1:length(allbounds{i})
        rectangle('Position',[(allbounds{i}(j)-3)*1.5 i 6*1.5 1],'FaceColor',pal(i,:),'LineStyle','none');
    end
end
xlim([0 880]);
set(gca,'YDir','reverse');
xlabel('Seconds');
