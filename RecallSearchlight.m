function RecallSearchlight(job_id)
if (ischar(job_id))
    job_id = str2double(job_id);
end

[x_slice, y_slice, nScenes] = ind2sub([17 21 5], job_id);
x_slice = x_slice*3+4;
y_slice = y_slice*3+4;
nScenes = nScenes*10;
fname = [num2str(x_slice) '_' num2str(y_slice) '_' num2str(nScenes) '_top.mat'];

if (exist(fullfile('TO_RS',fname),'file'))
    return;
end

disp(['Producing ' fname]);

% coord_slice, data_slice, recall_slice
load(fullfile('Janice','RecallSearchlight',[num2str(x_slice) '_' num2str(y_slice)]));
movie_group = mean(data_slice,3);

T = size(movie_group,2);
nSubj = 17;
nPerm = 100;
radius = 3;
step = 3;

% Output vars
allSLinds = {};
LL = zeros(0,nSubj,nPerm+1);

if (isempty(coord_slice))
    save(fullfile('TO_RS',fname), 'allSLinds', 'LL');
    return;
end

coord_dict = zeros(max(coord_slice(:,1))+10,max(coord_slice(:,2))+10,max(coord_slice(:,3))+10);
coord_dict(sub2ind(size(coord_dict),coord_slice(:,1),coord_slice(:,2),coord_slice(:,3))) = 1:size(coord_slice,1);

tStart = tic;
zcenters = (1+radius):step:(max(coord_slice(:,3))-radius);
zcenters = zcenters(end)+step;
for zcenter = zcenters
    SLinds = reshape(coord_dict(x_slice+(-radius:radius), y_slice+(-radius:radius), zcenter+(-radius:radius)),1,[]);
    if (sum(SLinds>0) <= 1)
        continue;
    end
    allSLinds{end+1} = SLinds(SLinds>0);

    % First is real, rest are scrambles
    gammas = zeros(T,nScenes,nPerm+1);

    % Learn (soft) movie segmentation
    [~, ~, ~, loggamma] = HMM_BW('trainingData',movie_group(allSLinds{end},:),'nScenes',nScenes);
    gammas(:,:,1) = exp(loggamma);

    sceneV = zeros(nScenes,nPerm+1);
    sceneV(:,1) = SceneVariance(data_slice(allSLinds{end},:,:),gammas(:,:,1));

    % Randomly reorder the segments
    rng(1);
    for i = 1:nPerm
        rp = randperm(nScenes);
        gammas(:,:,i+1) = gammas(:,rp,1);
        sceneV(:,i+1) = sceneV(rp,1);
    end

    LL = cat(1,LL,zeros(1,nSubj,nPerm+1));
    for i = 1:(nPerm+1)
        % Refit boundaries on recall data
        for s = 1:nSubj
            LL(end,s,i) = HMM_BW('trainingData',movie_group(allSLinds{end},:),'trainingSegments',gammas(:,:,i),...
                                 'sceneVariance',sceneV(:,i), 'testingData',recall_slice{s}(allSLinds{end},:));
        end
    end

    tElapsed = toc(tStart);
    fracComplete = (zcenter-zcenters(1))/(zcenters(end)-zcenters(1));
    disp([num2str(x_slice) ',' num2str(y_slice) ',' num2str(zcenter) ': ' num2str(length(allSLinds{end})) ' vox, ETA=' num2str(tElapsed*(1-fracComplete)/fracComplete/60) ' mins']);
end

save(fullfile('TO_RS',fname), 'allSLinds', 'LL');
end