% load('Z:\chrisb\HMM\Asieh\SherlockMovie\PMC.mat');
% PMC = mean(PMC,3);
% PMC = PMC(:,1:400);

% v = VideoReader('C:\\Users\\chrisb\\OneDrive\\Sherlock_part1_imovie.m4v');
% 
% vidWidth = v.Width;
% vidHeight = v.Height;
% mov = struct('cdata',zeros(vidHeight,vidWidth,3,'uint8'),...
%     'colormap',[]);
% v.CurrentTime = 180;
% k = 1;
% while v.CurrentTime < 300
%     mov(k).cdata = readFrame(v);
%     k = k+1;
% end
% 
% [aud,Fs] = audioread('C:\\Users\\chrisb\\OneDrive\\Sherlock_part1_imovie.mp3');
% aud = aud((180*Fs):(300*Fs),:);
% 
% videoFWriter = vision.VideoFileWriter('C:\\Users\\chrisb\\OneDrive\\Sherlock_seg.avi','FrameRate', v.FrameRate,'FileFormat','avi','AudioInputPort',true,'VideoCompressor','MJPEG Compressor');
% for i=1:length(mov)
%     step(videoFWriter, mov(i).cdata, aud(round(1+(i-1)/v.FrameRate*Fs):round(i/v.FrameRate*Fs)));
% end
% release(videoFWriter);
% 
% 
% hf = figure;
% set(hf,'position',[150 150 vidWidth vidHeight]);
% 
% movie(hf,mov,1,v.FrameRate);

% [~,~,~,loggamma] = HMM_BW('trainingData',PMC,'nScenes',30);
% seg = exp(loggamma)*hsv(30);
% H = -exp(loggamma).*loggamma;
% H(isnan(H)) = 0;
% height = 20;
% seg = permute(repmat(seg,1,1,height),[3 1 2]);
% 
% f = figure('Color',[1 1 1],'Position',[100 100 640 100]);
% for s = 180:(1/25):(300-1/25)
%     TRs = (round(s/1.5)-10):(round(s/1.5)+10);
%     im = image(-10*1.5:1.5:10*1.5,1:20,seg(:,TRs,:));
%     set(im,'AlphaData',1-1.5*repmat(sum(H(TRs,:),2)',height,1));
%     set(gca,'YTick',[]);
%     xlabel('Time (seconds)');
%     line([0 0],[1 20],'Color','k','LineWidth',3);
%     
%     [im, cm] = rgb2ind(frame2im(getframe(f)),256);
%     imwrite(im,cm,sprintf('im_frames/segoverlay_%04d.png',round((s-180)*25+1)));
%     
%     drawnow;
% end


f = figure('Color',[1 1 1],'Position',[288         543        1630         668]);%[288   470   795   741]);

[~,ll,~,loggamma] = HMM_BW('trainingData',PMC,'trainingSegments',ones(size(PMC,2),30),'testingData',PMC,'sceneVariance',@(x) 4*0.98^(x-1));
llnew = ll;
step = 1;
rng(1);
pat_vox = randi(1751,50,1);
while step <= 90 %llnew >= ll
    ll = llnew;

    a1 = axes('Position', [0.1    0.88    0.8    0.07]);

    seg = exp(loggamma)*hsv(30);
    H = -exp(loggamma).*loggamma;
    H(isnan(H)) = 0;
    height = 20;
    seg = permute(repmat(seg,1,1,height),[3 1 2]);
    im = image(seg);
    set(im,'AlphaData',1-1.5*repmat(sum(H,2)',height,1));
    %set(im,'AlphaData',repmat(1-200*[0;sum(diff(squeeze(seg(1,:,:))).^2,2)]',height,1));
    title('Timepoint segmentation');
    %xlabel('Timepoints');
    set(gca,'YTick',[]);
%     colormap(hsv(30));
%     cb = colorbar('southoutside');
%     cb_pos = cb.Position;
%     cb.Position = [cb_pos(1)+cb_pos(3)/4 0.1 cb_pos(3)/2 cb_pos(4)];
%     cb.FontSize = 12;
%     cb.Label.String = 'Latent scene number';
%     cb.Label.FontSize = 12;
%     caxis([1 30]);
    box off;


    
    a2 = axes('Position', [0.1    0.07    0.4    0.68]);
    norm_gamma = bsxfun(@times,exp(loggamma),1./sum(exp(loggamma),1));
    scene_pat = PMC*norm_gamma;
    scene_std_final = [0.347518 0.250809 0.301781 0.283368 0.246671 0.233329 0.274317 0.306971 0.334344 0.277767 0.355870 0.310384 0.238526 0.288110 0.230373 0.246855 0.312657 0.235051 0.251089 0.400117 0.277262 0.386247 0.330055 0.290189 0.239175 0.229043 0.357287 0.350849 0.288493 0.389542]';
    scene_std = zeros(size(loggamma,2),1);
    for i = 1:size(loggamma,2)
        scene_std(i) = mean(std(PMC,norm_gamma(:,i),2));
    end
    scene_alpha = (-scene_std./scene_std_final+1)*6+1;
    im = imagesc(scene_pat(pat_vox,:));
    colormap(a2,jet);
    set(im,'AlphaData',[ones(length(pat_vox)-8,1);linspace(1,1/8,8)']*scene_alpha'); %((1-exp((1:length(pat_vox))/(length(pat_vox))))/2+1)'*
    set(gca,'xaxisLocation','top');
%     set(gca,'XColor',[1 1 1]);
%     set(gca,'YColor',[1 1 1]);
    set(gca,'XTick',[]);
    set(gca,'YTick',[]);
    ylabel('Voxels');
    box off;
    
    a4 = axes('Position', [0.1    0.755    0.4    0.03]);
    imagesc(permute(repmat(hsv(30),1,1,5),[3 1 2]));
    axis off
    title('Scene voxel patterns');
    
%     scenecorr = corr(PMC*bsxfun(@times,exp(loggamma),1./sum(exp(loggamma),1)));
%     Y = cmdscale(1-scenecorr);
%     if (step > 2)
%         [~,Y] = procrustes(Y_old(:,1:2), Y(:,1:2));
%     end
%     Y_old = Y;
%     %plot3(Y(:,1),Y(:,2),Y(:,3),'k');
%     %hold on;
%     h = scatter(Y(:,1),Y(:,2),100,1:size(Y,1),'filled');
%     colormap(gca,hsv(30));
%     %hold off;
%     axis([-1 1 -1 1]);
%     axis off
%     axis square;
% %     cb = colorbar;
% %     cb.FontSize = 12;
% %     ylabel(cb,'Latent scene number');
% %     cb.Label.FontSize = 12;
%     
%     drawnow;
%     h.MarkerHandle.FaceColorData = uint8( [h.MarkerHandle.FaceColorData(1:3,:);255*0.6*ones(1,size(h.MarkerHandle.FaceColorData,2))] );
%     title('Latent scene structure');
%     %view(-80+step,22);
%     
    
    a3 = axes('Position', [0.55 0.07 0.4 0.68]);
    GG = exp(loggamma)*exp(loggamma');
    [dx, dy] = gradient(GG);
    edges = sqrt(dx.^2+dy.^2);
    im = imagesc(corr(PMC));
    set(im,'AlphaData',1-4*edges);
    xlabel('Timepoints');
    ylabel('Timepoints');
    set(gca,'YDir','normal');
    axis square;
    colorbar;
    colormap(a3,parula);
    title('Timepoint correlations');

    drawnow;
    [im, cm] = rgb2ind(frame2im(getframe(f)),256);
    imwrite(im,cm,sprintf('im_frames/trainviz_%02d.png',step));
    
    [~,llnew,~,loggamma] = HMM_BW('trainingData',PMC,'trainingSegments',exp(loggamma),'testingData',PMC,'sceneVariance',@(x) 4*0.98^(x+step-1));
    step = step+1;
    delete(a1);
    delete(a2);
    delete(a3);
    delete(a4);
end

return;

% Show Hipp boundary
load('JanicePCC_bound47hipp');
startTR = round(((21*60+39)+1419)/1.5);
endTR = round(((22*60+13)+1419)/1.5);

diverge_col = PTdiverge(20,[-1 1]);
diverge_col(2,:) = diverge_col(8,:);
diverge_col(3,:) = diverge_col(9,:);
diverge_col(19,:) = diverge_col(13,:);
diverge_col(18,:) = diverge_col(12,:);
diverge_col(4:11,:) = repmat(diverge_col(10,:),8,1);
diverge_col(12:17,:) = repmat(diverge_col(10,:),6,1);

f = figure('Color',[1 1 1],'Position',[513         342        1695         464]);
step = 0;
for frame = (startTR*1.5+1.5):(1/25):(endTR*1.5)
    TR = round(frame/1.5);
   
    subplot(1,3,1);
    im = imagesc(((1:1976)-1824)*1.5,((1:1976)-1824)*1.5,corr(mean(PCC,3)));
    GG = exp(loggamma)*exp(loggamma');
    [dx, dy] = gradient(GG);
    edges = sqrt(dx.^2+dy.^2);
    set(im,'AlphaData',1-4*edges);
    axis(([startTR endTR startTR endTR]-1824)*1.5);
    set(gca,'YDir','normal');
    axis square;
    set(gca,'XTick',[]);
    set(gca,'YTick',[]);
    xlabel('Timepoints','FontSize',16);
    ylabel('Timepoints','FontSize',16);
%     line([0 0],([startTR endTR]-1824)*1.5,'Color',[1 1 1],'LineWidth',3);
%     line(([startTR endTR]-1824)*1.5,[0 0],'Color',[1 1 1],'LineWidth',3);
    title('PCC correlations','FontSize',16);
    rectangle('Position',[(TR-1824-0.5)*1.5 (TR-1824-0.5)*1.5 1.5 1.5],'FaceColor','k');
    
    a1 = axes('Position',[0.4 0.11 0.45 0.815]);
    axis([(startTR-1824)*1.5 (endTR-1824)*1.5 -1 1.7]);
    %line([0 0],[-1 1.7],'Color',[0 0 0],'LineWidth',2,'LineStyle','--');
    for t = startTR:endTR
        rectangle('Position',[(t-1824)*1.5 -1 1.5 2.7],'FaceColor',diverge_col(round((exp(loggamma(t,47))+exp(loggamma(t+1,47)))*19/2+1),:),'EdgeColor','none');
    end
    axis off;
    
    a2 = axes('Position',[0.4 0.11 0.45 0.815]);
    shadedErrorBar(((startTR-1824):(TR-1824))*1.5,mean(example(:,(startTR-1796):(TR-1796))),1.96*std(example(:,(startTR-1796):(TR-1796)))/sqrt(17));
    axis([(startTR-1824)*1.5 (endTR-1824)*1.5 -1 1.7]);
    set(gca,'Color','none');
    line([(startTR-1824)*1.5 (endTR-1824)*1.5],[0 0],'Color',[0 0 0],'LineWidth',2);
    box off;
	set(gca,'XTick',[]);
    set(gca,'YTick',[]);
    title('Hippocampal activity','FontSize',16);
    xlabel('Timepoints','FontSize',16);
    text(-17, 1.4, 'Scene 46','FontSize',20);
    text(11, 1.4, 'Scene 47','FontSize',20);
    
    drawnow;
    [im, cm] = rgb2ind(frame2im(getframe(f)),256);
    imwrite(im,cm,sprintf('im_frames/boundary_%03d.png',step));
    delete(a1);
    delete(a2);
    step = step+1;
end