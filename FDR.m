function qvals = FDR(pvals, adjust_m1)
% Port of AFNI mri_fdrize.c
% Set adjust_m1 to false to disable adjusting based on true positives
if (nargin < 2)
    adjust_m1 = true;
end

if any(isnan(pvals))
    disp(['Replacing ' num2str(sum(isnan(pvals))) ' nan pvals']);
    pvals(isnan(pvals)) = 1;
end
assert(all(pvals>=0) && all(pvals<=1));
MINP = 10^-15;
pvals = reshape(pvals,1,[]);

pvals(pvals < MINP) = MINP;
pvals(pvals == 1) = 1-MINP;
np = length(pvals);

qvals = zeros(np,1);
[sorted_pvals, sorted_ind] = sort(pvals, 'ascend');
qmin = 1.0;
for i = np:-1:1
    qval = (np * sorted_pvals(i))/i;
    if (qval > qmin)
        qval = qmin;
    else
        qmin = qval;
    end
    qvals(i) = qval;
end

if (adjust_m1)
    % Estimate number of true positives m1
    phist = histc(pvals, 0:0.05:1);
    phist = phist(1:20);
    sorted_phist = sort(phist(4:19), 'ascend');
    median4 = np - 20*([1 2 2 1]*sorted_phist(7:10)')/6;
    median6 = np - 20*([1 2 2 2 2 1]*sorted_phist(6:11)')/10;
    m1 = min(median4, median6);

    qfac = (np - m1)/np;
    if (qfac < 0.5)
        qfac = 0.25 + qfac^2;
    end

    qvals(sorted_ind) = qvals*qfac;
else
    qvals(sorted_ind) = qvals;
end
end