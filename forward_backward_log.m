function [loggamma, viterbi, LL] = forward_backward_log(logB, Pi, EndPi, P)
% Computes log gamma and viterbi paths for HMM, in log space
% Inputs:
%   logB: time by state matrix of log p(response t | state k)
%   Pi, EndPi: 1 by state vector of starting and ending probs (set to
%              uniform 1/k in most cases)
%   P: state by state transition matrix, P(i,j) = prob transitioning from
%      state i to j. sum(P,2) should be all ones
%      If P has an extra column, this is treated as a dummy absorbing state
%      that is never observed
%
% Outputs:
%   loggamma: time by state matrix of posterior probabilities
%   viterbi: time by 1 vector giving most likely state sequence
%   LL: log-likelihood of fit, for model comparison



if (size(P,2) == size(P,1) + 1)
    P = [P; [zeros(1,size(P,1)) 1]];
    Pi = [Pi 0];
    EndPi = [EndPi 0];
    logB = [logB -Inf(size(logB,1),1)];
    trim_gamma = true;
else
    trim_gamma = false;
end

T = size(logB,1);
K = size(logB,2);

% Forward pass
logscale=zeros(T,1);   % Normalization factor to avoid underflow
logalpha=zeros(T,K);
logbeta=zeros(T,K);

% Viterbi vars
logmaxprob = zeros(T,K);
prev = zeros(T,K);

for t=1:T
  if (t==1)
    logalpha(1,:) = log(Pi) + logB(1,:);
  else
    logalpha(t,:)=log(exp(logalpha(t-1,:))*P) + logB(t,:);
  end
  logscale(t)=LogSumExp(logalpha(t,:));
  logalpha(t,:)=logalpha(t,:) - logscale(t);
  
  if (t==1)
      logmaxprob(1,:) = logalpha(1,:);
  else
    [logmaxprob(t,:), prev(t,:)] = max(repmat(logmaxprob(t-1,:)',1,K) + log(P) + repmat(logB(t,:),K,1));
    logmaxprob(t,:) = logmaxprob(t,:) - LogSumExp(logmaxprob(t,:));
  end
end

logmaxprob(T,:) = logmaxprob(T,:) + log(EndPi);
viterbi = zeros(T,1);
[~, viterbi(T)] = max(logmaxprob(T,:));
for t = (T-1):-1:1
    viterbi(t) = prev(t+1,viterbi(t+1));
end

% Backward pass
logbeta(T,:)=log(EndPi) - logscale(T);
for t=T-1:-1:1
  obs_weighted = logbeta(t+1,:) + logB(t+1,:);
  offset = max(obs_weighted);
  logbeta(t,:)=offset + log(exp(obs_weighted-offset)*(P')) - logscale(t); 
end

loggamma = logalpha + logbeta; 
loggamma=bsxfun(@minus, loggamma,LogSumExp(loggamma,2));

LL = sum(logscale(1:(T-1))) + LogSumExp(logalpha(T,:) + logscale(T) + log(EndPi));

if (trim_gamma)
    loggamma = loggamma(:,1:(end-1));
end
end

function LSE = LogSumExp(x, dim)
% Compute log(sum(exp(x))) for very negative x
if nargin < 2
    [~,dim] = find(size(x)-1,1);
end
offset = max(x, [], dim);
LSE = offset + log(sum(exp(bsxfun(@minus,x,offset)), dim));
end