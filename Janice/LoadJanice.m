function alldata = LoadJanice()
addpath('../NifTI');

mask = load_nii(fullfile('..','ROIs','MNI_caez_PGap.nii.gz'));
maskinds = find(mask.img(:));

dirprefix = 'C:\\Users\\chrisb\\Desktop\\LocalCache\\'; %'/scratch/janice/sherlock_movie/smoothed_6mm/'; %'Y:\\janice\\sherlock_movie\\smoothed_6mm\\';

%alldata = cell(17,1); 
alldata = zeros(1253,1976,17);
for i = 1:17
    disp(num2str(i));
    %alldata{i} = ImportSubj([dirprefix 'sherlock_recall_s' num2str(i) '.nii.gz'],maskinds);
    alldata(:,:,i) = ImportSubj([dirprefix 'sherlock_movie_s' num2str(i) '.nii.gz'],maskinds);
end

% nsubj = size(alldata,3);
% nvox = size(alldata,1);
% ISC = zeros(nvox,nsubj);
% for s = 1:nsubj
%     group = mean(alldata(:,:,1:nsubj ~= s),3);
%     ISC(:,s) = corr_columns(group',alldata(:,:,s)');
% end
% ISC = mean(ISC,2);

load('Movie/group_ISC25.mat','ISC');

[x, y, z] = ind2sub(size(mask.img),maskinds);

ISCthresh = 0.25;
coords = [x(ISC>ISCthresh) y(ISC>ISCthresh) z(ISC>ISCthresh)];
% alldata = alldata(ISC>ISCthresh,:,:);
for i = 1:17
    alldata{i} = alldata{i}(ISC>ISCthresh,:);
end
end


function data = ImportSubj(fname, maskinds)
data = load_nii(fname);
data = reshape(data.img,[],size(data.img,4));
if (nargin > 1)
    data = data(maskinds,:);
end
end