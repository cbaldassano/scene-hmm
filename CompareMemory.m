function CompareMemory(movie, viewers, nonviewers)

nScenes = 37;

% Select voxels with good ISC in all modalities
ISC_movie = zeros(1,size(movie,1));
ISC_viewers = zeros(1,size(viewers,1));
ISC_nonviewers = zeros(1,size(nonviewers,1));
nSubj = size(movie,3);
for i = 1:nSubj
    group = mean(movie(:,:,~ismember(1:nSubj,i)),3);
    ISC_movie = ISC_movie + corr_columns(group',movie(:,:,i)')/(nSubj-1);
    group = mean(viewers(:,:,~ismember(1:nSubj,i)),3);
    ISC_viewers = ISC_viewers + corr_columns(group',viewers(:,:,i)')/(nSubj-1);
    group = mean(nonviewers(:,:,~ismember(1:nSubj,i)),3);
    ISC_nonviewers = ISC_nonviewers + corr_columns(group',nonviewers(:,:,i)')/(nSubj-1);
end
highISCvox = ISC_movie>0.1 & ISC_viewers>0.1 & ISC_nonviewers>0.1;
movie = movie(highISCvox,:,:);
viewers = viewers(highISCvox,:,:);
nonviewers = nonviewers(highISCvox,:,:);

nSubj = size(viewers,3);
TM = size(movie,2);
TA = size(viewers,2);

[~, ~, ~, group_loggamma_viewers]  = HMM_BW('trainingData',{mean(movie,3), mean(viewers,3)}, 'nScenes', nScenes);
[~, ~, ~, group_loggamma_nonviewers]  = HMM_BW('trainingData',{mean(movie,3), mean(nonviewers,3)}, 'nScenes', nScenes);

viewer_prod = exp(group_loggamma_viewers{1})*exp(group_loggamma_viewers{2})';
nonviewer_prod = exp(group_loggamma_nonviewers{1})*exp(group_loggamma_nonviewers{2})';
viewer_pdf = bsxfun(@rdivide,viewer_prod,sum(viewer_prod));
nonviewer_pdf = bsxfun(@rdivide,nonviewer_prod,sum(nonviewer_prod));
viewer_expect = sum(viewer_pdf.*repmat((1:TM)',1,TA));
nonviewer_expect = sum(nonviewer_pdf.*repmat((1:TM)',1,TA));
mean(viewer_expect-nonviewer_expect)

rng(1);
nNull = 100;
expect_diff_null = zeros(nNull,1);
allsubj = cat(3,viewers,nonviewers);
parfor n = 1:nNull
%     if (mod(n,10)==1)
%         disp(['Calc null dist ' num2str(n) '/' num2str(nNull)]);
%     end
    group1_nview = 8+randi(2)-1;
    viewer_perm = randperm(17);
    nonviewer_perm = randperm(17);
    group1_subj = [viewer_perm(1:group1_nview) 17+nonviewer_perm(1:(17-group1_nview))];
    group2_subj = [viewer_perm((group1_nview+1):17) 17+nonviewer_perm((17-group1_nview+1):17)];

    [~, ~, ~, group_loggamma1]  = HMM_BW('trainingData',{mean(movie,3), mean(allsubj(:,:,group1_subj),3)}, 'nScenes', nScenes);
    [~, ~, ~, group_loggamma2]  = HMM_BW('trainingData',{mean(movie,3), mean(allsubj(:,:,group2_subj),3)}, 'nScenes', nScenes);

    group1_prod = exp(group_loggamma1{1})*exp(group_loggamma1{2})';
    group2_prod = exp(group_loggamma2{1})*exp(group_loggamma2{2})';
    group1_pdf = bsxfun(@rdivide,group1_prod,sum(group1_prod));
    group2_pdf = bsxfun(@rdivide,group2_prod,sum(group2_prod));
    group1_expect = sum(group1_pdf.*repmat((1:TM)',1,TA));
    group2_expect = sum(group2_pdf.*repmat((1:TM)',1,TA));
    expect_diff_null(n) = mean(group1_expect-group2_expect);
    disp(num2str(expect_diff_null(n)));
end

% viewer_ll = zeros(nSubj,2);
% nonviewer_ll = zeros(nSubj,2);
% viewer_loggamma = cell(nSubj,1);
% nonviewer_loggamma = cell(nSubj,1);
% viewer_viterbi = cell(nSubj,1);
% nonviewer_viterbi = cell(nSubj,1);
% 
% ISC_movie = zeros(1,size(movie,1));
% ISC_viewers = zeros(1,size(viewers,1));
% ISC_nonviewers = zeros(1,size(viewers,1));
% nSubj = size(movie,3);
% for i = 1:nSubj
%     group = mean(movie(:,:,~ismember(1:nSubj,i)),3);
%     ISC_movie = ISC_movie + corr_columns(group',movie(:,:,i)')/(nSubj);
%     group = mean(viewers(:,:,~ismember(1:nSubj,i)),3);
%     ISC_viewers = ISC_viewers + corr_columns(group',viewers(:,:,i)')/(nSubj);
%     group = mean(nonviewers(:,:,~ismember(1:nSubj,i)),3);
%     ISC_nonviewers = ISC_nonviewers + corr_columns(group',nonviewers(:,:,i)')/(nSubj);
% end
% allISC = ISC_movie.*ISC_viewers.*ISC_nonviewers;
% [~,ISCorder] = sort(allISC,'descend');
% nonnan = find(~isnan(allISC(ISCorder)),1,'first');
% nvox = 300;
% movie = movie(ISCorder(nonnan:(nonnan+nvox-1)),:,:);
% viewers = viewers(ISCorder(nonnan:(nonnan+nvox-1)),:,:);
% nonviewers = nonviewers(ISCorder(nonnan:(nonnan+nvox-1)),:,:);

% for i = 1:nSubj
%     disp(['Fitting subjects, ' num2str(i) '/' num2str(nSubj)]);
% 
%     [~, viewer_ll(i,:), ~, viewer_loggamma{i}, viewer_viterbi{i}]  = ...
%         HMM_BW('trainingData',{mean(movie(:,:,1:nSubj ~= i),3), viewers(:,:,i)}, 'nScenes', nScenes);
%     
%     [~, nonviewer_ll(i,:), ~, nonviewer_loggamma{i}, nonviewer_viterbi{i}]  = ...
%         HMM_BW('trainingData',{mean(movie(:,:,1:nSubj ~= i),3), nonviewers(:,:,i)}, 'nScenes', nScenes);
% end
% 
% 
% viewer_prod = zeros(nSubj,TM,TA);
% nonviewer_prod = zeros(nSubj,TM,TA);
% for i = 1:nSubj
%     viewer_prod(i,:,:) = exp(viewer_loggamma{i}{1})*exp(viewer_loggamma{i}{2})';
%     nonviewer_prod(i,:,:) = exp(nonviewer_loggamma{i}{1})*exp(nonviewer_loggamma{i}{2})';
% end
% 
% viewer_pdf = bsxfun(@rdivide,squeeze(sum(viewer_prod,1)),sum(squeeze(sum(viewer_prod,1)),1));
% nonviewer_pdf = bsxfun(@rdivide,squeeze(sum(nonviewer_prod,1)),sum(squeeze(sum(nonviewer_prod,1)),1));
% viewer_expect = sum(viewer_pdf.*repmat((1:TM)',1,TA));
% nonviewer_expect = sum(nonviewer_pdf.*repmat((1:TM)',1,TA));
% 
% rng(1);
% nNull = 100;
% expect_diff_null = zeros(nNull,1);
% all_prod = cat(1,viewer_prod,nonviewer_prod);
% for n = 1:nNull
%     if (mod(n,50)==1)
%         disp(['Calc null dist ' num2str(n) '/' num2str(nNull)]);
%     end
%     all_prod_scram = all_prod(randperm(2*nSubj),:,:);
%     group1_pdf = bsxfun(@rdivide,squeeze(sum(all_prod_scram(1:nSubj,:,:),1)),sum(squeeze(sum(all_prod_scram(1:nSubj,:,:),1)),1));
%     group2_pdf = bsxfun(@rdivide,squeeze(sum(all_prod_scram((nSubj+1):2*nSubj,:,:),1)),sum(squeeze(sum(all_prod_scram((nSubj+1):2*nSubj,:,:),1)),1));
%     group1_expect = sum(group1_pdf.*repmat((1:TM)',1,TA));
%     group2_expect = sum(group2_pdf.*repmat((1:TM)',1,TA));
%     expect_diff_null(n) = mean(group1_expect-group2_expect);
% end


% imagesc(1.5*(1:TA),1.5*(1:TM),viewer_pdf-nonviewer_pdf); set(gca,'YDir','normal');
% xlabel('Audio (seconds)');
% ylabel('Movie (seconds)');
% hold on;
% plot(1.5*(1:TA),1.5*viewer_expect,'LineStyle','--','LineWidth',3,'Color',[0 0 0]);
% plot(1.5*(1:TA),1.5*nonviewer_expect,'LineStyle',':','LineWidth',3,'Color',[1 1 1]);
% hold off;
% legend('With memory','Without memory');

colors = PTPalette(2);
hold off
im = image(1.5*(1:TA),1.5*(1:TM),...
    repmat(nonviewer_pdf*100,1,1,3).*permute(repmat(colors(1,:),TM,1,TA),[1 3 2]) + repmat(1-nonviewer_pdf*100,1,1,3).*permute(repmat([1 1 1],TM,1,TA),[1 3 2]));
set(im,'AlphaData',exp(nonviewer_pdf/0.005)-1);
hold on;
im = image(1.5*(1:TA),1.5*(1:TM),...
    repmat(viewer_pdf*100,1,1,3).*permute(repmat(colors(2,:),TM,1,TA),[1 3 2]) + repmat(1-viewer_pdf*100,1,1,3).*permute(repmat([1 1 1],TM,1,TA),[1 3 2]));
set(im,'AlphaData',exp(viewer_pdf/0.005)-1);
set(gca,'YDir','normal');
xlabel('Audio (seconds)');
ylabel('Movie (seconds)');
plot(1.5*(1:TA),1.5*viewer_expect,'LineStyle','--','LineWidth',3,'Color',[0 0 0]);
plot(1.5*(1:TA),1.5*nonviewer_expect,'LineStyle',':','LineWidth',3,'Color',[1 1 1]);
hold off;
legend('With memory','Without memory');

disp(['Memory group leads by ' num2str(1.5*mean(viewer_expect-nonviewer_expect)) ' seconds, p=' num2str(sum(expect_diff_null > mean(viewer_expect-nonviewer_expect))/nNull)]);
end