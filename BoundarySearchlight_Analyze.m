function BoundarySearchlight_Analyze(hipp_allsubsdata)

load('Janice\Movie\group_ISC25.mat','coords');

allSLinds = {};
SLmeancoords = [];
fit_ll = [];
scene_entropy = [];

nv = 22602;
T = 1976;

vox_entropy = zeros(nv,T);
SL_counts = zeros(nv,1);
for z = 10:3:46
    slice_data = load(fullfile('TO_BS',[num2str(z) '_50.mat']));
    allSLinds = [allSLinds slice_data.allSLinds];
    for sl = 1:length(slice_data.allSLinds)
        SLmeancoords = [SLmeancoords; mean(coords(slice_data.allSLinds{sl},:),1)];
        vox_entropy(slice_data.allSLinds{sl},:) = vox_entropy(slice_data.allSLinds{sl},:) + repmat(slice_data.scene_entropy(sl,:),length(slice_data.allSLinds{sl}),1);
        SL_counts(slice_data.allSLinds{sl}) = SL_counts(slice_data.allSLinds{sl}) + 1;
    end
    fit_ll = [fit_ll slice_data.fit_ll];
    scene_entropy = [scene_entropy; slice_data.scene_entropy];
end

vox_entropy = bsxfun(@times, vox_entropy, 1./SL_counts);

%Z = WardLinkage(single(vox_entropy));
%load('BS_Wardclust','Z');
% z = cluster(Z,'maxclust',18);
% z(z>8) = 0;
% nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
% nMaps = 1;
% nii_template.hdr.dime.dim(5) = nMaps;
% nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
% for v = 1:nv
%     nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = z(v);
% end
% save_nii(nii_template, 'Outputs/seg_clust8.nii.gz');
% 
% clust_means = zeros(max(z),size(vox_entropy,2));
% clust_peaks = cell(max(z),1);
% for c = 1:max(z)
%     clust_means(c,:) = mean(vox_entropy(z==c,:),1);
%     [~, clust_peaks{c}] = findpeaks(clust_means(c,:),'SortStr','descend','NPeaks',50);
% end

entropy_peaks = cell(nv,1);
for v = 1:nv
    [~, entropy_peaks{v}] = findpeaks(vox_entropy(v,:), 'SortStr', 'descend', 'NPeaks', 50);
end

avg_win = 55;
hipp_windows = zeros(T, 17, avg_win);
for s = 1:17
    subj_hipp = zscore(mean(hipp_allsubsdata.(['s' num2str(s)]).movie));
    for b = 1:T
        win_min = b-(avg_win-1)/2;
        if (win_min < 1)
            start_pad = NaN(1,1-win_min);
            win_min = 1;
        else
            start_pad = [];
        end

        win_max = b+(avg_win-1)/2;
        if win_max > T
            end_pad = NaN(1,win_max-T);
            win_max = T;
        else
            end_pad = [];
        end
        hipp_windows(b,s,:) = [start_pad subj_hipp(win_min:win_max) end_pad];
    end
end

z_peak = zeros(nv,1);
p_peak = zeros(nv,1);
for v = 1:nv
    subj_means = squeeze(mean(hipp_windows(entropy_peaks{v},:,:),'omitnan'));
    peak_diffs = mean(subj_means(:,28:32),2)-mean(subj_means(:,[1:27 33:55]),2);
    z_peak(v) = mean(peak_diffs)/std(peak_diffs);
    [~,p_peak(v)] = ttest(peak_diffs,0,'tail','right');
end
z_peak(isnan(p_peak)) = 0;
p_peak(isnan(p_peak)) = 1;

nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
nMaps = 2;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),1) = z_peak(v);
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),2) = -1*log10(p_peak(v));
end
save_nii(nii_template, 'hipp_peak.nii.gz');

lTPJ = z_peak>1.8 & coords(:,1) >= 13 & coords(:,1) <=19 & coords(:,2) >= 19 & coords(:,2) <= 28 & coords(:,3) >= 29 & coords(:,3) <= 36;
lTPJ_entropy = mean(vox_entropy(lTPJ,:));
HippCorr(lTPJ_entropy, hipp_allsubsdata);

rng(1);
vox_samp = randperm(nv,1000);
[COEFF, ~, ~, ~, ~, MU] = pca(vox_entropy(vox_samp,:),'NumComponents',3);
Y_vox = bsxfun(@minus,vox_entropy,MU)*COEFF;

mds_scaled = zeros(nv,3);
for i = 1:nv
    for j = 1:3
        mds_scaled(i,j) = sum(Y_vox(:,j)<Y_vox(i,j))/nv;
    end
end

nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
nMaps = 3;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = mds_scaled(v,:);
end
save_nii(nii_template, 'Outputs/seg_mds.nii.gz');

% 
% nSL = size(scene_entropy,1);
% 
% negcorr = 1-corr(scene_entropy');
% Y_corr = squareform(negcorr);
% Z_corr = linkage(Y_corr,'ward');
% %corr_order = optimalleaforder(Z_corr, Y_corr);
% %imagesc(negcorr(corr_order,corr_order));
% z = cluster(Z_corr,'maxclust',10);
% 
% mds_corr = cmdscale(negcorr);
% mds_corr_scaled = zeros(nSL,3);
% for i = 1:nSL
%     for j = 1:3
%         mds_corr_scaled(i,j) = sum(mds_corr(:,j)<mds_corr(i,j))/nSL;
%     end
% end
% 
% scatter3(SLmeancoords(:,1),SLmeancoords(:,2),SLmeancoords(:,3),30,mds_corr_scaled,'filled');
% scatter3(SLmeancoords(:,1),SLmeancoords(:,2),SLmeancoords(:,3),30,z,'filled');
% 
% for i = 1:nSL
%     peak_z(i) = HippCorr(scene_entropy(i,:), allsubsdata);
% end
% 
% % 
% pair_seg = sparse(nSL,size(scene_entropy,2)^2);
% for s = 1:nSL
%     [~,peaks] = findpeaks(scene_entropy(s,:));
%     boundaries = [1;peaks(:);size(scene_entropy,2)];
%     sl_seg = zeros(size(scene_entropy,2));
%     for i = 1:(length(boundaries)-1)
%         sl_seg(boundaries(i):boundaries(i+1),boundaries(i):boundaries(i+1)) = 1;
%     end
%     pair_seg(s,:) = sparse(reshape(sl_seg,1,[]));
% end
% 
% 
% Y = pdist(pair_seg,'jaccard');
% Z = linkage(Y,'average');
% seg_corr_order = optimalleaforder(Z, Y);
% imagesc(scene_entropy(seg_corr_order,:));
% 
% scatter(Y,Y_corr);


end

