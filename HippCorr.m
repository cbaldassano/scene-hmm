function subj_mean_triggered_avg = HippCorr(boundary_bool, hipp_data)
%boundary_bool is Tx1 or cell(Sx1) of Tsx1
avg_win = 21;
if (isstruct(hipp_data))
    nSubj = length(fields(hipp_data));
elseif (iscell(hipp_data))
    nSubj = length(hipp_data);
else
    nSubj = size(hipp_data,3);
end
subj_mean_triggered_avg = zeros(nSubj,avg_win);
%example = zeros(17,55);
for s = 1:nSubj
    if (isstruct(hipp_data))
        f = fields(hipp_data);
        subj_hipp = zscore(mean(hipp_data.(f{s}).movie));
    elseif (iscell(hipp_data))
        subj_hipp = zscore(mean(hipp_data{s}));
    else
        subj_hipp = zscore(mean(hipp_data(:,:,s)));
    end
    triggered_avg = zeros(0,avg_win);
    if iscell(boundary_bool)
        locs = find(boundary_bool{s});
        T = size(boundary_bool{s},1);
    else
        locs = find(boundary_bool);
        T = size(boundary_bool,1);
    end
    for b = locs'; %find(LocalMax(scene_entropy,10))' %find(scene_entropy) %  %) %find(LocalMax(scene_entropy(s,:),10))'
        win_min = b-(avg_win-1)/2;
        if (win_min < 1)
            start_pad = NaN(1,1-win_min);
            win_min = 1;
        else
            start_pad = [];
        end

        win_max = b+(avg_win-1)/2;
        if win_max > T
            end_pad = NaN(1,win_max-T);
            win_max = T;
        else
            end_pad = [];
        end

        triggered_avg = [triggered_avg; [start_pad subj_hipp(win_min:win_max) end_pad]];
    end
    %example(s,:) = triggered_avg(47,:);
    subj_mean_triggered_avg(s,:) = mean(triggered_avg,'omitnan');
end
% figure('Color',[1 1 1],'Position',[76    -7   869   435]);
% shadedErrorBar((-(avg_win-1)/2:(avg_win-1)/2)*1.5,mean(subj_mean_triggered_avg),1.96*std(subj_mean_triggered_avg)/sqrt(17));
% ylim([-0.35 0.35]);
% line([0 0],[-1 1],'Color','k','LineWidth',2,'LineStyle','--');
% hold on;
% shadedErrorBar((-(avg_win-1)/2:(avg_win-1)/2)*1.5,mean(subj_mean_triggered_avg),1.96*std(subj_mean_triggered_avg)/sqrt(17));
% xlabel('Seconds before/after HMM boundary');
% ylabel('Avg hippocampcal activity');
% box off
% 
% peak_diffs = mean(subj_mean_triggered_avg(:,28:32),2)-mean(subj_mean_triggered_avg(:,[1:27 33:55]),2);
% z_peak = mean(peak_diffs)/std(peak_diffs);
end