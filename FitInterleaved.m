function FitInterleaved()
% Determine number of scenes
% load('Interleaved/PCC_allsubj');
% nScenes = 10:5:50;
% nSubj = 18;
% w = 5; % TR difference for window
% 
% within_min_across = zeros(length(nScenes),nSubj);
% for scene_i = 1:length(nScenes)
%     disp(['Scenes = ' num2str(nScenes(scene_i))]);
%     for s = 1:nSubj
%         [~, ~, ~, loggamma]  = HMM_BW('trainingData',mean(allsubj(:,:,1:nSubj ~= s),3),'nScenes',nScenes(scene_i));
%         [~,scenelabel] = max(loggamma,[],2);
% 
%         windiffs = zeros(length(scenelabel)-w,1);
%         for t = 1:(length(scenelabel)-w)
%             windiffs(t) = corr2(allsubj(:,t,s),allsubj(:,t+w,s));
%         end
%         same_scene = scenelabel(1:(length(scenelabel)-w)) == scenelabel((1+w):end);
%         within_min_across(scene_i,s) = mean(windiffs(same_scene)) - mean(windiffs(~same_scene));
%         disp(['  s=' num2str(s) ': ' num2str(within_min_across(scene_i,s))]);
%     end
% end
% save('Outputs/Interleaved_loo','within_min_across','nScenes');
% Peaks at 35

load('Interleaved/PCC_3shift');
[~, ~, ~, loggamma]  = HMM_BW('trainingData',PCC,'nScenes',35);
[~,scenelabel] = max(loggamma,[],2);
story_bounds = diff(story);

t = tabulate(scenelabel);
scene_lengths = t(:,2);

rng(1);
nPerm = 1000;
win = 5;
bound_hist = zeros(win*2+1,nPerm+1);

for p = 1:(nPerm+1)
    bounds = diff(scenelabel);
    for i = find(story_bounds(:))'
        bound_hist(:,p) = bound_hist(:,p) + bounds((i-win):(i+win));
    end
    scene_perm = scene_lengths(randperm(length(scene_lengths)));
    scenelabel = repelem(1:length(scene_lengths),scene_perm)';
end

shadedErrorBar(-win:win,mean(bound_hist(:,2:end),2),std(bound_hist(:,2:end),[],2))
hold on;
bar(-win:win,bound_hist(:,1))
xlabel('TRs around human boundary')
ylabel('# HMM boundaries')

TRrange = (win-2):(win-2+6);
disp(['Null within 3 TRs=' num2str(mean(sum(bound_hist(TRrange,2:end)))) ' +- ' num2str(std(sum(bound_hist(TRrange,2:end))))]);
disp(['HMM: ' num2str(sum(bound_hist(TRrange,1))) ' p=' num2str(mean(sum(bound_hist(TRrange,2:end))>=18))]);
end