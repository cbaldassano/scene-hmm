function AcrossModalitySearchlight_Analyze()

load('Asieh\SherlockMovie\highISC_group.mat','coords');

allSLinds = {};

nScenes = 2:5:72;
nPerm=100;
LL = zeros(0,length(nScenes),nPerm+1);
nv = size(coords,1);
for z = 9:3:48
    disp(num2str(z));
    slice_data = load(fullfile('TO_AS',[num2str(z) '_2.mat']), 'allSLinds');
    for sl = 1:length(slice_data.allSLinds)
        if (any(cellfun(@(x) isequal(x,slice_data.allSLinds{sl}), allSLinds)))
            continue;
        end
        allSLinds{end+1} = slice_data.allSLinds{sl};
        LL = cat(1,LL, zeros(1,length(nScenes),nPerm+1));
        
        for ns = 1:length(nScenes)
            scene_data = load(fullfile('TO_AS',[num2str(z) '_' num2str(nScenes(ns)) '.mat']));
            LL(end,ns,:) = scene_data.test_LL(sl,:);
        end
    end
end

vox_LL = SLtoVox(allSLinds, LL, nv);
vox_LL(isnan(vox_LL)) = 0;
vox_LL = GaussSmooth(coords, vox_LL, 2);
vox_LLz = zeros(nv,length(nScenes));
for ns = 1:length(nScenes)
    vox_LLz(:,ns) = NullZscore(vox_LL(:,ns,1),squeeze(vox_LL(:,ns,2:end)));
end

vox_LLz(isnan(vox_LLz)) = 0;

[maxz, max_scene_i] = max(vox_LLz,[],2);
p = min((1-normcdf(maxz))*15,1);
q = FDR(p,false);
sigvox = q<10^-5;

nii_template.hdr = load_nii_hdr('Z:\\Asieh\\ListenerProject\\analysis\\subjects\\BT_021115\\SherlockMovie\\trans_filtered_func_data.nii');
nMaps = 2;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = [maxz(v) -log10(q(v))];
end

save_nii(nii_template, 'Outputs/Across.nii.gz');


end

