function d = DiceSim(bounds1, bounds2)
win = 3; % bounds within this window counted as a match

inter = 0;
for i = 1:length(bounds1)
    if (any(abs(bounds2-bounds1(i))<=win))
        inter = inter+1;
    end
end

d = 2*inter/(length(bounds1)+length(bounds2));
end