function LeadSearchlight_MaxScene_Analyze()

load('Asieh\SherlockMovie\highISC_group.mat','coords');

nPerm = 100;
allSLinds = {};
audio2_lead = zeros(0,nPerm+1);
prod_diff = zeros(0,nPerm+1);
real_loggamma = {};
for z = 9:3:48
    disp(num2str(z));
    slice_data = load(fullfile('TO_AS',[num2str(z) '_2.mat']), 'allSLinds');
    loggamma = {};
    for rngseed = 1:20
        lead_data = load(fullfile('TO_LS',[num2str(z) '_' num2str(rngseed) '.mat']));
        loggamma = cat(2, loggamma, lead_data.loggamma);
    end
    for sl = 1:length(slice_data.allSLinds)
        if (any(cellfun(@(x) isequal(x,slice_data.allSLinds{sl}), allSLinds)))
            continue;
        end
        allSLinds{end+1} = slice_data.allSLinds{sl};
        real_loggamma = [real_loggamma loggamma(sl,1)];

        TM = 941;
        TA = 715;
        audio2_lead = cat(1,audio2_lead,zeros(1,nPerm+1));
        prod_diff = cat(1,prod_diff,zeros(1,nPerm+1));
        for p = 1:size(loggamma,2)
            prod_audio1 = exp(loggamma{sl,p}{1})*exp(loggamma{sl,p}{2})';
            prod_audio2 = exp(loggamma{sl,p}{1})*exp(loggamma{sl,p}{3})';
            prod_diff(end,p) = sum(sum((prod_audio1-prod_audio2).^2));
            audio1_pdf = bsxfun(@rdivide,prod_audio1,sum(prod_audio1));
            audio2_pdf = bsxfun(@rdivide,prod_audio2,sum(prod_audio2));
            audio1_expect = sum(audio1_pdf.*repmat((1:TM)',1,TA));
            audio2_expect = sum(audio2_pdf.*repmat((1:TM)',1,TA));
            audio2_lead(end,p) = mean(audio2_expect-audio1_expect);
        end
    end
end
prod_diff_vox = SLtoVox(allSLinds,prod_diff,5961);
prod_diff_vox(isnan(prod_diff_vox)) = 0;
prod_diff_smooth = GaussSmooth(coords, prod_diff_vox, 2);
z = NullZscore(prod_diff_smooth(:,1),prod_diff_smooth(:,2:end));
p=1-normcdf(z);
q=FDR(p);

vox_audio2_lead = SLtoVox(allSLinds,audio2_lead,5961);
vox_audio2_lead(isnan(vox_audio2_lead)) = 0;
vox_audio2_lead = GaussSmooth(coords, vox_audio2_lead, 2);

nii_template.hdr = load_nii_hdr('Z:\\Asieh\\ListenerProject\\analysis\\subjects\\BT_021115\\SherlockMovie\\trans_filtered_func_data.nii');
nMaps = 1;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
nv = size(coords,1);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = (q(v)<0.05)*(vox_audio2_lead(v)>0)*(vox_audio2_lead(v)*1.5);
end

save_nii(nii_template, 'Outputs/LeadSL.nii.gz');

% frontal SL=868, PMCbehind=991, PCCaheead = 877
SLind = 868;
TM = 941;
TA = 715;
prod_audio1 = exp(real_loggamma{SLind}{1})*exp(real_loggamma{SLind}{2})';
prod_audio2 = exp(real_loggamma{SLind}{1})*exp(real_loggamma{SLind}{3})';
im = imagesc(1.5*(1:TA),1.5*(1:TM),(prod_audio2>0.2)*2);
set(im,'AlphaData',prod_audio2>0.2);
hold on;
im = imagesc(1.5*(1:TA),1.5*(1:TM),prod_audio1>0.2);
set(im,'AlphaData',prod_audio1>0.2);
hold on;
im = imagesc(1.5*(1:TA),1.5*(1:TM),(prod_audio2>0.2)*2);
set(im,'AlphaData',prod_audio2);
hold off;
xlim([0 1.5*TA])
set(gca,'YDir','normal');
set(gca,'XTick',0:500:(1.5*TA));
set(gca,'YTick',0:500:(1.5*TM));
set(gca,'FontSize',16);
xlabel('Audio');
ylabel('Movie');
caxis([0 2.5]);

load('Z:\chrisb\HMM\Asieh\SherlockMovie\segment.mat');
load('Z:\chrisb\HMM\Asieh\SherlockAudio1\segment.mat');
OverlayBlocks(Sherlock_movie_segment,Sherlock_audio_segment);
axis square
end

