
% Prob boundary k of K scenes at time t of T
T = 500;
K = 10;
p = zeros(K-1,T);
for k = 1:(K-1)
    for t = k:(T-K+k+1)
        p(k,t) = nchoosek(t-1,k-1)*nchoosek(T-t,K-k-1)/nchoosek(T,K-1);
    end
end

pal = PTPalette(10);
hold off;
for k = 1:(K-1)
    plot(p(k,:),'Color',pal(k,:),'LineWidth',2.5);
    hold on;
end
box off;
xlabel('Timepoints');
ylabel('Probability');

% Overlapping boundaries
% for k = 1:(K-1)
%     for t = 1:T
%         p(k,t) = (1/T)^(K-1)*nchoosek(K-1,k-1)*(K-k)*(t-1)^(k-1)*(T-t)^(K-k-1);
%     end
% end

% % Prob k=t and k+1=t+L
% p_t_len = zeros(K-2,T,T);
% for k = 1:(K-2)
%     for t = 1:T
%         for L = 1:(T-t)
%             p_t_len(k,t,L) = (1/T)^(K-1)*nchoosek(K-1,k-1)*(K-k)*(K-k-1)*(t-1)^(k-1)*(T-t-L)^(K-k-2);
%         end
%     end
% end
% 
% % Prob k=t and k+1=t+L | k=t
% p_t_len_cond = zeros(K-2,T,T);
% for k = 1:(K-2)
%     for t = 1:T
%         for L = 1:(T-t)
%             p_t_len_cond(k,t,L) = (K-k-1)*(T-t-L)^(K-k-2)/(T-t)^(K-k-1);
%         end
%     end
% end
% 
% % Prob k to k+1 =L
% p_len = squeeze(sum(p_t_len,2));