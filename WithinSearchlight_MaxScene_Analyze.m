function WithinSearchlight_MaxScene_Analyze()

load(fullfile('Janice','Movie','ISC25.mat'),'coords');

nSubj = 17;
nv = size(coords,1);
T = 1976;
nPerm = 100;

% Prepare hippocampal data
load(fullfile('Janice','Movie','hipp.mat'));
mean_hipp = mean(mean(hipp,3),1);


% Prepare human boundary data
load(fullfile('Janice','bounds_4observers.mat'));
near_human = zeros(1,T);
for t = 1:T
    if (sum(cellfun(@(x) any(abs(x-t)<=3), bounds))>=1)%(sum(cellfun(@(x) any(abs(x-t)<=3), bounds))>=2)
        near_human(t) = 1;
    end
end

% sig = 0.8; %Maximizes within-rater sim
% conv_human = zeros(4,T);
% for i = 1:4
%     conv_human(i,bounds{i}) = 1;
%     conv_human(i,:) = conv(conv_human(i,:), normpdf(round(-4*sig):round(4*sig),0,sig), 'same');
% end

% Similarity among raters
% bound_overlap = zeros(4,1001);
% for b1 = 1:4
%     locs = bounds{b1};
%     boundlabel = zeros(1,T);
%     boundlabel(locs) = 1;
%     boundlabel = cumsum(boundlabel(:))+1;
%     t = tabulate(boundlabel);
%     scene_lengths = t(:,2);
%     for p = 1:1001
%         conv_temp = zeros(1,T);
%         conv_temp(locs) = 1;
%         conv_temp = conv(conv_temp, normpdf((-4*sig):(4*sig),0,sig), 'same');
%         for b2 = setdiff(1:4,b1)
%             bound_overlap(b1,p) = bound_overlap(b1,p) + sum(conv_human(b2,:).*conv_temp)/sum(conv_temp);
%         end
%         
%         scene_perm = scene_lengths(randperm(length(scene_lengths)));
%         locs = cumsum(scene_perm(1:(end-1)));
%     end
% end
% mean(NullZscore(bound_overlap(:,1),bound_overlap(:,2:end)));

% Load data
SLinds = {};
entropy = zeros(0,T);
scenelabel = zeros(0,T);
for z = 9:3:48
    slice = load(fullfile('TO_WAS',[num2str(z) '.mat']));
    SLinds = [SLinds slice.allSLinds];
    entropy = [entropy; slice.entropy];
    scenelabel = [scenelabel; slice.scenelabel];
end

% Compute conv_bound, human overlap, hipp trigger
nSL = length(SLinds);
%conv_bound = zeros(nSL,T);
%human_overlap = zeros(nSL,nPerm+1);
hipp_diff = zeros(nSL,nPerm+1);
mean_nh = zeros(nSL,nPerm+1);

for i = 1:length(SLinds)
    if (mod(i,100)==1)
        disp([num2str(i) '/' num2str(length(SLinds))]);
    end
    [~,locs] = findpeaks(entropy(i,:),'NPeaks',max(scenelabel(i,:))-1,'SortStr','descend');
    boundlabel = zeros(1,T);
    boundlabel(locs) = 1;
    boundlabel = cumsum(boundlabel(:))+1;
    t = tabulate(boundlabel);
    scene_lengths = t(:,2);
    
    
%     for offset = -5:5
%         offsetlocs = locs+offset;
%         offsetlocs(offsetlocs>T | offsetlocs<1) = [];
%         nh_profile(i,offset+6) = mean(human_bound(offsetlocs));
%     end

%     conv_bound(i,locs) = 1;
%     conv_bound(i,:) = conv(conv_bound(i,:), normpdf((-4*sig):(4*sig),0,sig), 'same');
%     
    rng(1);
    for p = 1:(nPerm+1)
%         conv_temp = zeros(1,T);
%         conv_temp(locs) = 1;
%         conv_temp = conv(conv_temp, normpdf((-4*sig):(4*sig),0,sig), 'same');
%         for b = 1:4
%             human_overlap(i,p) = human_overlap(i,p) + sum(conv_human(b,:).*conv_temp)/sum(conv_temp);
%         end
%        

        mean_nh(i,p) = mean(near_human(locs));
    
        postTRs = unique(reshape([locs+1 locs+2 locs+3 locs+4 locs+5 locs+6 locs+7 locs+8 locs+9 locs+10],[],1));
        preTRs = unique(reshape([locs-1 locs-2 locs-3 locs-4 locs-5 locs-6 locs-7 locs-8 locs-9 locs-10],[],1));
        postTRs(postTRs>T) = [];
        preTRs(preTRs<1) = [];
        hipp_diff(i,p) = mean(mean_hipp(postTRs))-mean(mean_hipp(preTRs));
        
        scene_perm = scene_lengths(randperm(length(scene_lengths)));
        locs = cumsum(scene_perm(1:(end-1)));
    end
end


hipp_diff_vox = SLtoVox(SLinds, hipp_diff, nv);
hipp_diff_z = NullZscore(hipp_diff_vox(:,1),hipp_diff_vox(:,2:end));
hipp_diff_p = 1-normcdf(hipp_diff_z);
hipp_diff_q = FDR(hipp_diff_p);

mean_nh_vox = SLtoVox(SLinds, mean_nh, nv);
mean_nh_z = NullZscore(mean_nh_vox(:,1),mean_nh_vox(:,2:end));
mean_nh_p = 1-normcdf(mean_nh_z);
mean_nh_q = FDR(mean_nh_p,false);

% % MDS on best-scene conv_bound
% [COEFF, ~, ~, ~, ~, MU] = pca(conv_bound,'NumComponents',3);
% Y_SL = bsxfun(@minus,conv_bound,MU)*COEFF;
% Y_vox = SLtoVox(SLinds, Y_SL, nv);
% 
% mds_scaled = zeros(nv,3);
% for i = 1:nv
%     for j = 1:3
%         mds_scaled(i,j) = (sum(Y_vox(:,j)<Y_vox(i,j))+1)/nv;
%     end
% end
% 
% % Gaussian mixture
% rng(1);
% gm = fitgmdist(conv_bound,12,'CovarianceType','diagonal','SharedCovariance',true);
% P_SL = posterior(gm,conv_bound);
% P_vox = SLtoVox(SLinds, P_SL, nv);
% 
% palette = PTPalette(12);
% GM_colors = P_vox*palette;
% human_overlap_clust_z = zeros(10,1);
% for m = 1:12
%     rng(1);
%     [~,locs] = findpeaks(gm.mu(m,:));
%     overlap = zeros(nPerm+1,1);
%     conv_human = zeros(1,T);
%     conv_human(:,human_boundaries) = 1;
%     for p = 1:(nPerm+1)
%         conv_temp = zeros(1,T);
%         conv_temp(locs) = 1;
%         conv_temp = conv(conv_temp, normpdf((-4*sig):(4*sig),0,sig), 'same');
%         overlap(p) = sum(conv_human.*conv_temp)/sum(conv_temp);
% 
%         scene_perm = scene_lengths(randperm(length(scene_lengths)));
%         locs = cumsum(scene_perm(1:(end-1)));
%     end
%     human_overlap_clust_z(m) =  NullZscore(overlap(1), overlap(2:end)');
% end
    
% Output to NIFTI
nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
nMaps = 2;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = [mean_nh_vox(v) -log10(mean_nh_q(v))];
end
save_nii(nii_template, 'Outputs/frac_near_human.nii.gz');

nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
nMaps = 2;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = [hipp_diff_z(v) -log10(hipp_diff_q(v))];
end
save_nii(nii_template, 'Outputs/hipp_diff.nii.gz');

save('Outputs\WAS_MS_results','mean_nh_vox','hipp_diff_z');

% 
% nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
% nMaps = 3;
% nii_template.hdr.dime.dim(5) = nMaps;
% nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
% for v = 1:nv
%     nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = mds_scaled(v,:);
% end
% save_nii(nii_template, 'Outputs/seg_mds_maxscene.nii.gz');
% 
% 
% nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
% nMaps = 2;
% nii_template.hdr.dime.dim(5) = nMaps;
% nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
% for v = 1:nv
%     nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = [human_overlap_z_vox(v) -log10(human_overlap_q(v))];
% end
% save_nii(nii_template, 'Outputs/human_overlap.nii.gz');
% 
% nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
% nMaps = 2;
% nii_template.hdr.dime.dim(5) = nMaps;
% nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
% for v = 1:nv
%     nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = [peak_z_vox(v) -log10(peak_q(v))];
% end
% save_nii(nii_template, 'Outputs/hipp_5TRs_after.nii.gz');
% 
% nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
% nMaps = 3;
% nii_template.hdr.dime.dim(5) = nMaps;
% nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
% for v = 1:nv
%     nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = GM_colors(v,:);
% end
% save_nii(nii_template, 'Outputs/GMclust12.nii.gz');
end

