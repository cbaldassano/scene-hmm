function RecallSearchlight_Analyze()

load(fullfile('Janice','Movie','ISC25.mat'),'coords');

nSubj = 17;
nv = size(coords,1);
nPerm = 100;
scenes = 10:10:50;

% Load data
disp('Loading...');
SLinds = {};
LL = zeros(0,nSubj,length(scenes),nPerm+1);
for x = 7:3:55
    for y = 7:3:67
        load(fullfile('Janice','RecallSearchlight',[num2str(x) '_' num2str(y) '.mat']),'coord_slice');
        if (isempty(coord_slice))
            continue;
        end
        
        orig_inds = findrows(coord_slice, coords);
        ref = load(fullfile('TO_RS_complete',[num2str(x) '_' num2str(y) '_10.mat']));
        for i = 1:length(ref.allSLinds)
            SLinds = [SLinds {orig_inds(ref.allSLinds{i})}];
        end
        
        LL_slice = zeros(length(ref.allSLinds),nSubj,length(scenes),nPerm+1);
        for scene_i = 1:length(scenes)
            nScenes = scenes(scene_i);
            slice = load(fullfile('TO_RS_complete',[num2str(x) '_' num2str(y) '_' num2str(nScenes) '.mat']));
            LL_slice(:,:,scene_i,:) = slice.LL;
        end
        LL = cat(1,LL,LL_slice);
    end
end

LL_vox = SLtoVox(SLinds, LL, nv);
LL_smooth = GaussSmooth(coords,LL_vox,2);
LL_z = zeros(nv,nSubj,length(scenes));

for i = 1:nSubj
    for j = 1:length(scenes)
        LL_z(:,i,j) = NullZscore(LL_smooth(:,i,j,1),squeeze(LL_smooth(:,i,j,2:end)));
    end
end
[LL_z_max maxscene] = max(LL_z,[],3);
maxscene = mean(maxscene,2);
LL_z_max_sm = mean(LL_z_max,2);

p = (1-normcdf(LL_z_max_sm))*5;
q = FDR(p, false);
save('Outputs\RecallSL','LL','LL_smooth','LL_z','LL_z_max','LL_z_max_sm','maxscene','q');

% Compute LLz
% disp('Computing LLz..');
% nSL = length(SLinds);
% LL = zeros(nSL,nPerm+1);
% max_scene = zeros(nSL,1);
% 
% for i = 1:nSL
%     selected = zeros(nSubj, nPerm+1);
%     for loo = 1:nSubj
%         currLL = squeeze(LL(i,1:nSubj ~= loo,:,:));
%         currLL = squeeze(mean(bsxfun(@times, currLL, 1./abs(currLL(:,1,1)))));
%         currLLz = NullZscore(currLL(:,1), currLL(:,2:end));
%         [~,loomax] = max(currLLz);
%         max_scene(i) = max_scene(i) + (1/nSubj)*scenes(loomax);
%         selected(loo,:) = LL(i,loo,loomax,:)/abs(LL(i,loo,loomax,1));
%     end
%     LL_max(i,:) = mean(selected);
% end
% LL_max_vox = SLtoVox(SLinds, LL_max, nv);
% LLz_max_vox = NullZscore(LL_max_vox(:,1),LL_max_vox(:,2:end));
% max_scene_vox = SLtoVox(SLinds, max_scene, nv);
% p = 1-normcdf(LLz_max_vox);
% q = FDR(p, false);

nii_template.hdr = load_nii_hdr('C:\\Users\\chrisb\\Desktop\\LocalCache\\sherlock_movie_s1.nii');
nMaps = 3;
nii_template.hdr.dime.dim(5) = nMaps;
nii_template.img = zeros(nii_template.hdr.dime.dim(2),nii_template.hdr.dime.dim(3),nii_template.hdr.dime.dim(4),nMaps);
for v = 1:nv
    nii_template.img(coords(v,1),coords(v,2),coords(v,3),:) = [LL_z_max_sm(v) maxscene(v) -log10(q(v))];
end
save_nii(nii_template, 'Outputs/recall_LLz_max.nii.gz');

end

