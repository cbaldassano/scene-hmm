function Asieh_32scenes()

loaded = load(fullfile('Asieh','SherlockMovie','PMC.mat'));
movie = loaded.PMC;

loaded = load(fullfile('Asieh','SherlockAudio2','PMC.mat'));
viewer = loaded.PMC;

loaded = load(fullfile('Asieh','SherlockAudio1','PMC.mat'));
nonviewer = loaded.PMC;
clear loaded;
        
% Select voxels with good ISC in both modalities
ISC_movie = zeros(1,size(movie,1));
ISC_viewer = zeros(1,size(viewer,1));
ISC_nonviewer = zeros(1,size(viewer,1));
nSubj = size(movie,3);
for i = 1:nSubj
    group = mean(movie(:,:,~ismember(1:nSubj,i)),3);
    ISC_movie = ISC_movie + corr_columns(group',movie(:,:,i)')/(nSubj);
    group = mean(viewer(:,:,~ismember(1:nSubj,i)),3);
    ISC_viewer = ISC_viewer + corr_columns(group',viewer(:,:,i)')/(nSubj);
    group = mean(nonviewer(:,:,~ismember(1:nSubj,i)),3);
    ISC_nonviewer = ISC_nonviewer + corr_columns(group',nonviewer(:,:,i)')/(nSubj);
end
allISC = ISC_movie.*ISC_viewer.*ISC_nonviewer;
[~,ISCorder] = sort(allISC,'descend');
nonnan = find(~isnan(allISC(ISCorder)),1,'first');
nvox = 300;
movie = movie(ISCorder(nonnan:(nonnan+nvox-1)),:,:);
viewer = viewer(ISCorder(nonnan:(nonnan+nvox-1)),:,:);
nonviewer = nonviewer(ISCorder(nonnan:(nonnan+nvox-1)),:,:);

[~, ~, ~, loggamma_viewer] = HMM_BW('trainingData',{mean(movie,3) mean(viewer,3)},'nScenes',32);
[~, ~, ~, loggamma_nonviewer] = HMM_BW('trainingData',{mean(movie,3) mean(nonviewer,3)},'nScenes',32);
% [~, ~, ~, loggamma_audio] = HMM_BW('trainingData',{mean(viewer,3) mean(nonviewer,3)},'nScenes',22);

% meanPatterns_mv_m =  mean(movie,3)*bsxfun(@times,exp(loggamma_viewer{1}),1./sum(exp(loggamma_viewer{1}),1));
% meanPatterns_mv_v =  mean(viewer,3)*bsxfun(@times,exp(loggamma_viewer{2}),1./sum(exp(loggamma_viewer{2}),1));
% meanPatterns_mn_m =  mean(movie,3)*bsxfun(@times,exp(loggamma_nonviewer{1}),1./sum(exp(loggamma_nonviewer{1}),1));
% meanPatterns_mn_n =  mean(viewer,3)*bsxfun(@times,exp(loggamma_nonviewer{2}),1./sum(exp(loggamma_nonviewer{2}),1));

gammathresh = 0.35;

figure;

subplot(1,2,1);
imagesc(corr(mean(movie,3),mean(viewer,3)));
set(gca,'YDir','normal');
hold on;
gammaprod = exp(loggamma_viewer{1})*exp(loggamma_viewer{2})';
borders = [diff(gammaprod>gammathresh)~=0;zeros(1,715)] + [diff(gammaprod>gammathresh,[],2)~=0 zeros(941,1)];
[x,y] = ind2sub(size(borders),find(borders));
scatter(y,x,8,'r','filled');
hold off;
title('Simulfit movie and memory');

subplot(1,2,2);
imagesc(corr(mean(movie,3),mean(nonviewer,3)));
set(gca,'YDir','normal');
hold on;
gammaprod = exp(loggamma_nonviewer{1})*exp(loggamma_nonviewer{2})';
borders = [diff(gammaprod>gammathresh)~=0;zeros(1,715)] + [diff(gammaprod>gammathresh,[],2)~=0 zeros(941,1)];
[x,y] = ind2sub(size(borders),find(borders));
scatter(y,x,8,'r','filled');
hold off;
title('Simulfit movie and naive');

end