function HippEncodingRecall()
movie = cell(3,1);
loaded = load(fullfile('Janice','Movie','PG'));
movie{1} = loaded.PG;
loaded = load(fullfile('Janice','Movie','PCC'));
movie{2} = loaded.PCC;
loaded = load(fullfile('Janice','Movie','A1'));
movie{3} = loaded.A1;

recall = cell(3,1);
loaded = load(fullfile('Janice','Recall','PG'));
recall{1} = loaded.PG;
loaded = load(fullfile('Janice','Recall','PCC'));
recall{2} = loaded.PCC;
loaded = load(fullfile('Janice','Recall','A1'));
recall{3} = loaded.A1;
clear loaded

hipp_movie = load(fullfile('Janice','Movie','hipp'));
hipp_movie = hipp_movie.hipp;

scenes = 10:5:60;
nSubj = size(movie{1},3);
nPerm = 100;
T = 1976;

LL = zeros(length(movie),length(scenes),nSubj,nPerm+1);
for R = 1:length(movie)
    disp(num2str(R));
    parfor scene_ind = 1:length(scenes)
        disp(['   ' num2str(scene_ind)]);
        % First is real, rest are scrambles
        gammas = zeros(T,scenes(scene_ind),nPerm+1);

        % Learn (soft) movie segmentation
        [~, ~, ~, loggamma] = HMM_BW('trainingData',mean(movie{R},3),'nScenes',scenes(scene_ind));
        gammas(:,:,1) = exp(loggamma);

        sceneV = zeros(scenes(scene_ind),nPerm+1);
        sceneV(:,1) = SceneVariance(movie{R},gammas(:,:,1));

        % Randomly reorder the segments
        rng(1);
        for i = 1:nPerm
            rp = randperm(scenes(scene_ind));
            gammas(:,:,i+1) = gammas(:,rp,1);
            sceneV(:,i+1) = sceneV(rp,1);
        end

        scene_LL = zeros(nSubj, nPerm+1);
        for i = 1:(nPerm+1)
            % Refit boundaries on recall data
            for s = 1:nSubj
                scene_LL(s,i) = HMM_BW('trainingData',mean(movie{R},3),'trainingSegments',gammas(:,:,i),...
                                     'sceneVariance',sceneV(:,i), 'testingData',recall{R}{s});
            end
        end
        LL(R,scene_ind,:,:) = scene_LL;
    end
end
save('Outputs/Recall_LL','LL');

%LL_norm = bsxfun(@times, LL, 1./abs(LL(:,:,:,1)));
LLz_group = zeros(length(movie),length(scenes));
for R = 1:length(movie)
    for i = 1:length(scenes)
        LLz_group(R,i) = NullZscore(mean(squeeze(LL(R,i,:,1))),mean(squeeze(LL(R,i,:,2:end))));
    end
end
% Max around 20-25 scenes
h = plot(scenes, LLz_group');
pal = PTPalette(4);
set(h(1),'Color',pal(1,:),'LineWidth',2);
set(h(2),'Color',pal(4,:),'LineWidth',2);
set(h(3),'Color',pal(2,:),'LineWidth',2);
line([min(scenes) max(scenes)],[1.645 1.645]);
legend('PG','PCC','A1');
xlabel('Scenes');
ylabel('Recall z vs. null');

loggamma = cell(length(movie),1);
for R = 1:length(movie)
    simuldata = cell(2*nSubj,1);
    for i = 1:nSubj
        simuldata{i} = mean(movie{R},3);
    end
    for i = (nSubj+1):(2*nSubj)
        simuldata{i} = recall{R}{i-nSubj};
    end

    [~,~,~,loggamma{R}] = HMM_BW('trainingData',simuldata,'nScenes',25);
end
save('Outputs\Recall_simulgamma','loggamma');

load('Janice\recall_segments.mat');
for R = 1:length(movie)
    figure;
    for i = 1:17
        subplot(3,6,i);
        imagesc(1.5*(1:size(loggamma{R}{i+17},1)),1.5*(1:size(loggamma{R}{i},1)),exp(loggamma{R}{i})*exp(loggamma{R}{i+17})');
        OverlayBlocks(movie_TRs{i},recall_TRs{i});
    end
end
figure
for R = 1:3
    subplot(1,3,R);
    im = imagesc(1.5*(1:size(loggamma{R}{16+17},1)),1.5*(1:size(loggamma{R}{16},1)),exp(loggamma{R}{16})*exp(loggamma{R}{16+17})');
    OverlayBlocks(movie_TRs{16},recall_TRs{16});
    set(im,'AlphaData',sqrt(exp(loggamma{R}{16})*exp(loggamma{R}{16+17})')*2);
    hold off;
    set(gca,'YDir','normal');
    set(gca,'XTick',0:500:(1.5*size(loggamma{R}{16+17},1)));
    set(gca,'YTick',0:500:(1.5*size(loggamma{R}{17},1)));
    set(gca,'FontSize',16);
    xlabel('Recall');
    ylabel('Movie');
    if (R==1)
        col = pal(1,:);
    elseif (R==2)
        col = pal(4,:);
    else
        col = pal(2,:);
    end
    colormap(gca,[linspace(1,col(1),100)' linspace(1,col(2),100)' linspace(1,col(3),100)']);
end

for R = 1:3
    [~,movie_scene] = max(loggamma{R}{1},[],2);
    movie_bound = find(diff(movie_scene));

    post_TRs = 10;
    pre_TRs = 10;
    hipp_encode = zeros(nSubj,length(movie_bound));
    for s = 1:nSubj
        zHipp = zscore(mean(hipp_movie(:,:,s)));
        for b = 1:length(movie_bound)
            hipp_encode(s,b) = mean(zHipp(movie_bound(b):min(movie_bound(b)+post_TRs,T))) - mean(zHipp(max(movie_bound(b)-pre_TRs,1):movie_bound(b)));
        end
    end

    encode_time = mean(exp(loggamma{R}{1}));

    recall_time = zeros(nSubj,length(movie_bound)+1);
    for s = 1:nSubj
        recall_time(s,:) = mean(exp(loggamma{R}{s+nSubj}));
    end


    figure
    scatter(mean(hipp_encode),mean(recall_time(:,1:24)),'filled');
    for i = 1:length(movie_bound)
        hipp_bar = std(hipp_encode(:,i))/sqrt(nSubj);%*tinv(0.95,nSubj-1);
        recall_bar = std(recall_time(:,i))/sqrt(nSubj);%*tinv(0.95,nSubj-1);
        line(mean(hipp_encode(:,i))+hipp_bar*[-1 1],mean(recall_time(:,i))*[1 1]);
        line(mean(hipp_encode(:,i))*[1 1],mean(recall_time(:,i))+recall_bar*[-1 1]);
    end
    xlabel('Hippocampal boost during encoding')
    ylabel('Reactivation at recall')
    xlim([-1 1]);
    ylim([0.015 0.06]);

    % Group resampling
    nSamp = 1000;
    bootstrap_corr = zeros(nSamp,3);
    xstep = -1:0.01:1;
    fitlines = zeros(nSamp,length(xstep));
    rng(1);
    for i = 1:nSamp
        samp = randi(nSubj,nSubj,1);
        bootstrap_corr(i,1) = corr2(mean(hipp_encode(samp,:)),mean(recall_time(samp,1:24)));
        bootstrap_corr(i,2) = corr2(mean(hipp_encode(samp,:)),mean(recall_time(samp,2:25)));
        bootstrap_corr(i,3) = corr2(encode_time,mean(recall_time(samp,:)));

        B = regress(mean(recall_time(samp,1:24))',[mean(hipp_encode(samp,:))' ones(24,1)]);
        fitlines(i,:) = xstep*B(1)+B(2);
    end
    hold on;
    shadedErrorBar(xstep,mean(fitlines),std(fitlines)*1.96);
    mean(bootstrap_corr<0) % 0.0020    0.8670    0.1590, 0.0420    0.1130    0.0500, 0.3330    0.4300         0
end

% boundary_bool = false(T,1);
% boundary_bool(movie_bound) = true;
% subj_mean_triggered_avg = HippCorr(boundary_bool, hipp_movie);
% shadedErrorBar(-27:27,mean(subj_mean_triggered_avg),std(subj_mean_triggered_avg)/sqrt(17)*tinv([0.9967],17-1))
% xlabel('Time relative to boundary');
% xlabel('Time relative to angular gyrus boundary');
% ylabel('Mean hippocampal activity');

% subj_corrs = zeros(nSubj,3); % hipp-prevscene, hipp-nextscene, encodetime-recalltime
% for s = 1:nSubj
%     subj_corrs(s,1) = corr2(hipp_encode(s,:),recall_time(s,1:length(movie_bound)));
%     subj_corrs(s,2) = corr2(hipp_encode(s,:),recall_time(s,2:(length(movie_bound)+1)));
%     subj_corrs(s,3) = corr2(encode_time,recall_time(s,:));
% end
% 
% [~,p] = ttest(atanh(subj_corrs),0,'tail','right')

% subj_recall = zeros(nSubj,length(movie_bound));
% for s = 1:nSubj
%     zHipp = zscore(mean(hipp_recall{s}));
%     
%     H = -exp(loggamma{s+nSubj}).*loggamma{s+nSubj};
%     H(isnan(H)) = 0;
%     entropy = sum(H,2);
%     [~,recall_bound] = findpeaks(entropy,'NPeaks',size(loggamma{s+nSubj},2),'SortStr','descend','MinPeakDistance',5);
%     for b = 1:length(recall_bound)
%         subj_recall(s,b) = mean(zHipp(recall_bound(b):min(recall_bound(b)+post_TRs,length(zHipp))));
%     end
% end

end