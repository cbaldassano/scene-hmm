function gamma = LabelToGamma(labels)
K = max(labels);
T = length(labels);
gamma = zeros(T,K);
valid_TR = (1:T)';
valid_TR(labels == 0) = [];
labels(labels == 0) = [];
on_inds = sub2ind(size(gamma),valid_TR,labels);
gamma(on_inds) = 1;
end