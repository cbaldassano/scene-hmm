% Modification of Matlab "LINKAGEOLD" function to only merge elements
%   that are spatially adjacent, as specified by adj_list
function Z = WardLinkage(X)

% Compute squared euclidean distance Y between rows
Qx = repmat(dot(X,X,2),1,size(X,1));
Y = Qx+Qx'-2*(X*X');
Y(1:(size(Y,1)+1):size(Y,1)^2) = 0; % Remove numerical errors on diagonal
Y = squareform(Y);

N = size(X,1);
valid_clusts = true(N,1);
col_limits = cumsum((N-1):-1:1);

m = ceil(sqrt(2*size(Y,2))); % (1+sqrt(1+8*n))/2, but works for large n
if isa(Y,'single')
   Z = zeros(m-1,3,'single'); % allocate the output matrix.
else
   Z = zeros(m-1,3); % allocate the output matrix.
end

% During updating clusters, cluster index is constantly changing, R is
% a index vector mapping the original index to the current (row, column)
% index in Y.  C denotes how many points are contained in each cluster.
C = zeros(1,2*m-1);
C(1:m) = 1;
R = 1:m;

for s = 1:(m-1)
   if (mod(s,100)==1)
       disp(num2str(s));
   end
   % Find closest pair of clusters
   [v, k] = min(Y,[],'omitnan');
   
   j = find(k <= col_limits, 1, 'first');
   i = N - (col_limits(j) - k);

   Z(s,:) = [R(i) R(j) v]; % Add row to output linkage

   % Update Y. In order to vectorize the computation, we need to compute
   % all the indices corresponding to cluster i and j in Y, denoted by I
   % and J.
   U = valid_clusts;
   U([i j]) = 0;
   I = PdistInds(i, N, U);
   J = PdistInds(j, N, U);

   Y(I) = ((C(R(U))+C(R(i))).*Y(I) + (C(R(U))+C(R(j))).*Y(J) - ...
    C(R(U))*v)./(C(R(i))+C(R(j))+C(R(U)));
   
   U(i) = 1;
   J = PdistInds(j, N, U);
   Y(J) = NaN;
   
   valid_clusts(j) = 0;
   
   % update m, N, R
   C(m+s) = C(R(i)) + C(R(j));
   R(i) = m+s;
end

Z(:,3) = sqrt(Z(:,3));

end

% Compute positions in distance vector (for NxN matrix) for a given row. Results
%   are masked by the valid_flags boolean vector
function I = PdistInds(row, N, valid_flags)

if (row > 1)
    inds1 =[(row-1) (row-1)+cumsum((N-2):-1:(N-row+1))];
    I = [inds1 0 (inds1(end)+N-row+1):(inds1(end)+2*N-2*row)];
else
    I = 0:(N-1);
end

I = I(valid_flags);
    
end
