function DDA()
addpath('NifTI');
long = load_nii(fullfile('DDA_TRW','DDAlongTRW_0.2.nii'));
med = load_nii(fullfile('DDA_TRW','DDAmedTRW_0.2.nii'));
short = load_nii(fullfile('DDA_TRW','DDAshortTRW_0.2.nii'));

long = reshape(long.img,[],1);
med = reshape(med.img,[],1);
short = reshape(short.img,[],1);

max_s = load_nii(fullfile('Outputs','max_scene.nii.gz'));
max_s = reshape(max_s.img,[],1);

long = long(max_s > 0);
med = med(max_s > 0);
short = short(max_s > 0);
max_s = max_s(max_s > 0);

histogram(max_s(long==1 | med==1),30,'Normalization','pdf','BinLimits',[min(max_s) max(max_s)],'EdgeColor','none');
hold on;
histogram(max_s(short==1),30,'Normalization','pdf','BinLimits',[min(max_s) max(max_s)],'EdgeColor','none');

xlabel('Optimal number of events');
legend('Long/med','Short');

% fwd = load_nii(fullfile('DDA_TRW','fwdavg_12ss.nii'));
% crs = load_nii(fullfile('DDA_TRW','crsavg_11ss.nii'));
% ts = reshape(fwd.img,[],1) - reshape(crs.img,[],1);
% 
% ts = ts(max_s > 0);
% max_s = max_s(max_s > 0);
end
