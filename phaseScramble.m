function x_scram = phaseScramble(x)
% Phase scrambles each row of x
x_scram = zeros(size(x));
for r = 1:size(x,1)
    RandomPhase = angle(fft(rand(1,size(x,2))));
    row_fft = fft(x(r,:));
    amp = abs(row_fft);
    phase = angle(row_fft) + RandomPhase;
    x_scram(r,:) = real(ifft(amp.*exp(sqrt(-1)*phase)));
end
end