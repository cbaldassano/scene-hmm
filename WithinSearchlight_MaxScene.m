function WithinSearchlight_MaxScene(job_id) %z_slice, loo, nScenes)
% if (ischar(z_slice))
%     z_slice = str2double(z_slice);
%     loo = str2double(loo);
%     nScenes = str2double(nScenes);
% end
if (ischar(job_id))
    job_id = str2double(job_id);
end

z_slice = job_id*3+6;
if (exist(fullfile('TO_WAS',[num2str(z_slice) '.mat']),'file'))
    return;
end

disp(['Producing ' num2str(z_slice) '.mat']);

load(fullfile('Janice','Movie','group_ISC25.mat'));

T = size(groupdata,2);
nSubj = 17;
scenes = 10:10:120;


ref = load(fullfile('TO_WAS',[num2str(z_slice) '_1_10.mat']));
allSLinds = ref.allSLinds;
% Output vars
scenelabel = zeros(length(allSLinds),T);
entropy = zeros(length(allSLinds),T);

max_scene_i = zeros(length(allSLinds),nSubj);
for loo = 1:nSubj
    within_min_across = zeros(length(allSLinds),length(scenes));
    for scene_i = 1:length(scenes)
        nScenes = scenes(scene_i);
        slice = load(fullfile('TO_WAS',[num2str(z_slice) '_' num2str(loo) '_' num2str(nScenes) '.mat']));
        within_min_across(:,scene_i) = slice.within(:,1)-slice.across(:,1);
    end
    [~,max_scene_i(:,loo)] = max(within_min_across,[],2);
end
max_scene = round(10*mean(max_scene_i,2)); 

tStart = tic;
for i = 1:length(allSLinds)
    [~, ~, ~, loggamma] = HMM_BW('trainingData',groupdata(allSLinds{i},:),'nScenes',max_scene(i));
    [~,scenelabel(i,:)] = max(loggamma,[],2);

    H = -exp(loggamma).*loggamma;
    H(isnan(H)) = 0;
    entropy(i,:) = sum(H,2);

    tElapsed = toc(tStart);
    fracComplete = i/length(allSLinds);
    disp([num2str(i) '/' num2str(length(allSLinds)) ': ' num2str(length(allSLinds{i})) ' vox, ETA=' num2str(tElapsed*(1-fracComplete)/fracComplete/60) ' mins']);
end

save(fullfile('TO_WAS',[num2str(z_slice) '.mat']), 'allSLinds', 'scenelabel', 'entropy', 'max_scene');
end