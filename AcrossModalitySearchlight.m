function AcrossModalitySearchlight(z_slice, nScenes)
disp(['AcrossModalitySearchlight, z_slice=' z_slice ', nScenes=' nScenes]);

movie = load(fullfile('Asieh','SherlockMovie','highISC_group.mat'));
audio = load(fullfile('Asieh','SherlockAudio1','highISC_group.mat'));

radius = 3;
step = 3;
nScram = 100;

allSLinds = {};
test_LL = zeros(0,nScram+1);
meancorr = zeros(0,nScram+1);

coord_dict = zeros(max(movie.coords(:,1))+10,max(movie.coords(:,2))+10,max(movie.coords(:,3))+10);
coord_dict(sub2ind(size(coord_dict),movie.coords(:,1),movie.coords(:,2),movie.coords(:,3))) = 1:size(movie.coords,1);

tStart = tic;
xcenters = (1+radius):step:(max(movie.coords(:,1))-radius);
ycenters = (1+radius):step:(max(movie.coords(:,2))-radius);
for xcenter = xcenters
    for ycenter = ycenters
        SLinds = [];
        for x = -radius:radius
            for y = -radius:radius
                for z = -radius:radius
                    SLinds = [SLinds coord_dict(xcenter+x, ycenter+y, str2double(z_slice)+z)];
                end
            end
        end
        if (sum(SLinds>0) <= 1)
            continue;
        end
        
        allSLinds{end+1} = SLinds(SLinds>0);

        gammas = zeros(size(movie.groupmean,2),str2double(nScenes),nScram+1);
        [~, ~, end_scale, loggamma] = HMM_BW('trainingData',movie.groupmean(allSLinds{end},:),'nScenes',str2double(nScenes));
        gammas(:,:,1) = exp(loggamma);
        
        rng(1);
        for i = 1:nScram
            gammas(:,:,i+1) = gammas(:,randperm(size(gammas,2)),1);
        end

        %sceneV = mean(SceneVariance(movie.groupmean(allSLinds{end},:),gammas(:,:,1)));
        LL = zeros(1,nScram+1);
        MC = zeros(1,nScram+1);
        for i = 1:(nScram+1)
            [LL(i), ~, ~, loggamma] = ...
                HMM_BW('trainingData',movie.groupmean(allSLinds{end},:),'trainingSegments',gammas(:,:,i),'sceneVariance',end_scale,... %sceneV, ...
                            'testingData',audio.groupmean(allSLinds{end},:));

            % Calculate scene correlations with group
            meanPatterns_group =  movie.groupmean(allSLinds{end},:)*bsxfun(@times,gammas(:,:,i),1./sum(gammas(:,:,i),1));
            meanPatterns_loo =  audio.groupmean(allSLinds{end},:)*bsxfun(@times,exp(loggamma),1./sum(exp(loggamma),1));
            MC(i) = mean(corr_columns(meanPatterns_group,meanPatterns_loo));
        end
        test_LL = [test_LL;LL];
        meancorr = [meancorr;MC];

        
        tElapsed = toc(tStart);
        fracComplete = ((xcenter-xcenters(1))/step*length(ycenters) + (ycenter-ycenters(1))/step)/(length(xcenters)*length(ycenters));
        disp([num2str(xcenter) ',' num2str(ycenter) ',' num2str(z_slice) ': ' num2str(length(allSLinds{end})) ' vox, ETA=' num2str(tElapsed*(1-fracComplete)/fracComplete/60) ' mins']);
        %disp([num2str(end_scale) ',' num2str(sceneV) ',' num2str(NullZscore(LL(1),LL(2:end)))]);
    end
end

save(fullfile('TO_AS',[z_slice '_' nScenes '.mat']), 'allSLinds', 'test_LL', 'meancorr');
end