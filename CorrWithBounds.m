function CorrWithBounds(data, nScenes)
figure;
[~, ~, ~, loggamma] = HMM_BW('trainingData',data,'nScenes',nScenes);
[~,scenelabel] = max(loggamma,[],2);

imagesc(1.5:1.5:(1.5*size(data,2)),1.5:1.5:(1.5*size(data,2)),corr(data));
OverlayBlocks(scenelabel,scenelabel);

xlabel('Timepoints');
ylabel('Timepoints');
set(gca,'YDir','normal');
axis square;
colorbar;
colormap(parula);
caxis([-1 1]);
title('Timepoint correlations');
end