function LOO_acrossModality(loo, nScenes)

nScram = 100;
dirname = 'TO_LOO_acrossModality';
loo = str2double(loo);
if (strcmp('22Annot',nScenes))
    nScenes = 22;
    use_human_annot = true;
else
    nScenes = str2double(nScenes);
    use_human_annot = false;
end

tic;
experiment = 'Asieh'; %'Janice';%
datasets = {'SherlockAudio1' 'SherlockAudio2'};%{'Recall'};%
rois = {'A1' 'PMC'};%{'PCC'};%
test_LL = zeros(nScram+1,length(datasets),length(rois));
meancorr = zeros(nScram+1,length(datasets),length(rois));
for dataset_ind = 1:length(datasets)
    for roi_ind = 1:length(rois)
        loaded = load(fullfile(experiment,'SherlockMovie',[rois{roi_ind} '.mat'])); %Movie
        movie = loaded.(rois{roi_ind});
        clear loaded;

        loaded = load(fullfile(experiment,datasets{dataset_ind},[rois{roi_ind} '.mat']));
        audio = loaded.(rois{roi_ind});
        audio = audio(:,:,loo);%audio{loo};%
        clear loaded;

        train_subj = setdiff(1:size(movie,3),loo);
        group_train = mean(movie(:,:,train_subj),3);
        T = size(movie,2);

        % First is real, rest are scrambles
        gammas = zeros(T,nScenes,nScram+1);

        if (use_human_annot)
            loaded = load(fullfile(experiment,'SherlockMovie','segment'));
            movie_seg = loaded.Sherlock_movie_segment;
            gammas(:,:,1) = LabelToGamma(movie_seg);
        else
            % Learn (soft) segmentation on all but one subject
            [~, ~, ~, loggamma] = HMM_BW('trainingData',group_train,'nScenes',nScenes,'PCAdims',-1);
            gammas(:,:,1) = exp(loggamma);
        end

        % Randomly reorder the segments
        rng(1);
        for i = 1:nScram
            gammas(:,:,i+1) = gammas(:,randperm(nScenes),1);
        end

        for i = 1:(nScram+1)
            % Refit boundaries on held out subject
            sceneV = SceneVariance(movie(:,:,train_subj),gammas(:,:,i));
            [test_LL(i,dataset_ind,roi_ind), ~, ~, loggamma] = ...
                HMM_BW('trainingData',group_train,'trainingSegments',gammas(:,:,i),'sceneVariance',sceneV, ...
                            'testingData',audio,'PCAdims',-1);

            % Calculate scene correlations with group
            meanPatterns_group =  group_train*bsxfun(@times,gammas(:,:,i),1./sum(gammas(:,:,i),1));
            meanPatterns_loo =  audio*bsxfun(@times,exp(loggamma),1./sum(exp(loggamma),1));
            meancorr(i,dataset_ind,roi_ind) = mean(corr_columns(meanPatterns_group,meanPatterns_loo));
        end

        disp([datasets{dataset_ind} ' ' rois{roi_ind} ' ' num2str(test_LL(1,dataset_ind,roi_ind)) ' ' num2str(meancorr(1,dataset_ind,roi_ind)) ' ' num2str(mean(test_LL(2:end,dataset_ind,roi_ind))) ' ' num2str(mean(meancorr(2:end,dataset_ind,roi_ind)))]);
        toc;
        if ~exist(dirname,'dir')
            mkdir(dirname);
        end
        if (use_human_annot)
            filename = [num2str(loo) '_' num2str(nScenes) 'Annot.mat'];
        else
            filename = [num2str(loo) '_' num2str(nScenes) '.mat'];
        end
        save([dirname '/' filename],'test_LL','meancorr');
    end
end

end