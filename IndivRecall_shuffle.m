loaded = load(fullfile('Janice','Movie','PCC.mat'));
movie = mean(loaded.PCC,3);
clear loaded;
loaded = load(fullfile('Janice','Recall','PCC.mat'));
recall = loaded.PCC;
loaded = load(fullfile('Janice','recall_segments.mat'));
movie_seg = loaded.movie_TRs;
recall_seg = loaded.recall_TRs;
clear loaded;
nScenes = 22;

[~, ~, end_scale, loggamma] = HMM_BW('trainingData',{movie, recall{17}},'nScenes',nScenes);

%[~, ~, ~, recall_gamma] = HMM_BW('trainingData',movie,'trainingSegments',exp(loggamma{1}),'testingData',recall{17},'sceneVariance',end_scale);
recall_shuffle = cell(100,1);
for i = 1:100
[~, ~, ~, recall_shuffle{i}] = HMM_BW('trainingData',movie,'trainingSegments',exp(loggamma{1}(:,randperm(22))),'testingData',recall{17},'sceneVariance',end_scale);
end

tracks = zeros(101,1976,1441);
for i = 1:101
    if (i>1)
        tracks(i,:,:) = exp(loggamma{1})*exp(recall_shuffle{i-1})';
    else
        tracks(i,:,:) = exp(loggamma{1})*exp(loggamma{2})';
    end
end

Tm = size(movie,2);
Tr = size(recall{17},2);

figure('Color',[1 1 1],'Position',[541   466   690   631]);
im = imagesc(1.5*(1:Tr),1.5*(1:Tm),squeeze(tracks(1,:,:)));
set(im,'AlphaData',sqrt(squeeze(tracks(1,:,:)))*3);
xlim([0 1.5*Tr])
set(gca,'YDir','normal');
set(gca,'XTick',0:1000:(1.5*Tr));
set(gca,'YTick',0:1000:(1.5*Tm));
set(gca,'FontSize',16);

figure('Color',[1 1 1],'Position',[541   466   690   631]);
im = image(1.5*(1:Tr),1.5*(1:Tm),max(1-repmat(squeeze(tracks(33,:,:)),1,1,3)*2,0.5));
set(im,'AlphaData',sqrt(squeeze(tracks(33,:,:)))*2);
hold on;
im = imagesc(1.5*(1:Tr),1.5*(1:Tm),squeeze(tracks(1,:,:)));
set(im,'AlphaData',sqrt(squeeze(tracks(1,:,:)))*3);
xlim([0 1.5*Tr])
set(gca,'YDir','normal');
set(gca,'XTick',0:1000:(1.5*Tr));
set(gca,'YTick',0:1000:(1.5*Tm));
set(gca,'FontSize',16);

figure('Color',[1 1 1],'Position',[541   466   690   631]);
im = image(1.5*(1:Tr),1.5*(1:Tm),max(1-repmat(squeeze(tracks(33,:,:)),1,1,3)*2,0.5));
set(im,'AlphaData',sqrt(squeeze(tracks(33,:,:)))*2);
hold on;
im = image(1.5*(1:Tr),1.5*(1:Tm),max(1-repmat(squeeze(tracks(35,:,:)),1,1,3)*2,0.5));
set(im,'AlphaData',sqrt(squeeze(tracks(35,:,:)))*2);
im = imagesc(1.5*(1:Tr),1.5*(1:Tm),squeeze(tracks(1,:,:)));
set(im,'AlphaData',sqrt(squeeze(tracks(1,:,:)))*3);
xlim([0 1.5*Tr])
set(gca,'YDir','normal');
set(gca,'XTick',0:1000:(1.5*Tr));
set(gca,'YTick',0:1000:(1.5*Tm));
set(gca,'FontSize',16);

OverlayBlocks(movie_seg{17}, recall_seg{17},[0 0 0]);