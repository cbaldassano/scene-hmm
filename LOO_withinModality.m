function LOO_withinModality(loo, nScenes)

nScram = 100;
dirname = 'TO_LOO_withinModality';
loo = str2double(loo);
nScenes = str2double(nScenes);

experiment = 'Asieh';%'Janice';
datasets = {'SherlockMovie' 'SherlockAudio1' 'SherlockAudio2'};%{'Movie'};%
rois = {'A1' 'PMC'};%{'PCC'};%
test_LL = zeros(nScram+1,length(datasets),length(rois));
meancorr = zeros(nScram+1,length(datasets),length(rois));
for dataset_ind = 1:length(datasets)
    for roi_ind = 1:length(rois)
        loaded = load(fullfile(experiment,datasets{dataset_ind},[rois{roi_ind} '.mat']));
        D = loaded.(rois{roi_ind});
        clear loaded;

        train_subj = setdiff(1:size(D,3),loo);
        group_train = mean(D(:,:,train_subj),3);
        T = size(D,2);

        % First is real, rest are scrambles
        gammas = zeros(T,nScenes,nScram+1);

        % Learn (soft) segmentation on all but one subject
        [~, ~, ~, loggamma] = HMM_BW('trainingData',group_train,'nScenes',nScenes,'PCAdims',-1);
        gammas(:,:,1) = exp(loggamma);

        % Generate random segmentations with same scene length distribution
        [~,maxscene] = max(gammas(:,:,1),[],2);
        t = tabulate(maxscene);
        sceneSizes = t(:,2);
        rng(1);
        for i = 1:nScram
            borders = [0;cumsum(sceneSizes(randperm(nScenes)))];
            for k = 1:nScenes
                gammas((borders(k)+1):borders(k+1),k,i+1) = 1;
            end
        end

        for i = 1:(nScram+1)
            % Calculate LL on held out subject
            sceneV = SceneVariance(D(:,:,train_subj),gammas(:,:,i));
            test_LL(i,dataset_ind,roi_ind) = ...
                HMM_BW('trainingData',group_train,'trainingSegments',gammas(:,:,i),'sceneVariance',sceneV, ...
                            'testingData',D(:,:,loo),'copySegments',true,'PCAdims',-1);

            % Calculate scene correlations with group
            meanPatterns_group =  group_train*bsxfun(@times,gammas(:,:,i),1./sum(gammas(:,:,i),1));
            meanPatterns_loo =  D(:,:,loo)*bsxfun(@times,gammas(:,:,i),1./sum(gammas(:,:,i),1));
            meancorr(i,dataset_ind,roi_ind) = mean(corr_columns(meanPatterns_group,meanPatterns_loo));
        end

        disp([datasets{dataset_ind} ' ' rois{roi_ind} ' ' num2str(test_LL(1,dataset_ind,roi_ind)) ' ' num2str(meancorr(1,dataset_ind,roi_ind)) ' ' num2str(mean(test_LL(2:end,dataset_ind,roi_ind))) ' ' num2str(mean(meancorr(2:end,dataset_ind,roi_ind)))]);
        if ~exist(dirname,'dir')
            mkdir(dirname);
        end
        filename = [num2str(loo) '_' num2str(nScenes) '.mat'];
        save([dirname '/' filename],'test_LL','meancorr');
    end
end

end